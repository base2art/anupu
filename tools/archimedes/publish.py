import subprocess, os, shutil, time
from task import Task
from msbuildutil import SLN

class Publish(Task):
    def __init__(self, logger):
        Task.__init__(self,logger)
        
    def _invokeInternal(self, params):
        production_dll_sources = params["production_dll_sources"]
        publish_destination = params["publish_destination"]
        self._copyToBinn(production_dll_sources, publish_destination)
        
    def _requiredParameters(self):
        return ["production_dll_sources", "publish_destination"]
    

    def _copyToBinn(self, paths, dest):
        j = os.path.join
        for path in paths:
            if os.path.exists(path):
                for fileName in os.listdir(path):
                    file = j(path, fileName)
                    if os.path.isfile(file) and (file.endswith(".dll") or file.endswith(".pdb") or file.endswith(".exe")):
                        #Hack cause i kept getting errors if i didn't add retries
                        self._makeDirIfDoesntExist(dest)
                        shutil.copy(file, j(dest,fileName))

                    
    def _makeDirIfDoesntExist(self, dest, triesRemaining = 5):
        try:
            if not os.path.isdir(dest):
                os.mkdir(dest)
        except WindowsError as we:
            time.sleep(0.5)
            print ("I'm tired!")
            if triesRemaining == 0:
                raise we
            self._makeDirIfDoesntExist(dest, triesRemaining - 1)


