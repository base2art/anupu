import os, shutil
from runner import Usage

class SLN:

    FileFilter = shutil.ignore_patterns('*.pyc','CVS','^.git','tmp','.svn',"*.cache","obj","bin", ".git")

    
    def GetCommonParams(additionalParams = {}):
        u = Usage()
        ret = {
            "mode": u.GetBuildMode(),
            "nunit_console":"C:/Program Files (x86)/NUnit 2.6/bin/nunit-console.exe",
            "publish_destination" : ".\\" + u.GetPublishDirectoryName()
            }
        for key,value in additionalParams.items():
            ret[key] = value
        return ret
    
    def GetTestDLLs(mode, solution):
        result = []
        lines = []
        with open(solution, 'r') as fh:
            lines = fh.readlines()
        for line in lines:
            if line.startswith("Project(\""):
                remaining = eval("(" + line[len("Project(\"{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}\") = "):].strip() + ")")
                
                if remaining[1].lower().startswith("tests"):
                    path = os.path.join(os.curdir, remaining[1])
                    
                    dirname = os.path.dirname(path)
                    dllName = os.path.basename(dirname)
                    j = os.path.join
                    path = j(j(dirname, "bin"), mode)
                    dllPath = j(path,remaining[0] + ".dll")
                    if os.path.exists(dllPath):
                        result.append(dllPath)
                    
        return result

    def GetProductionBinDirectories(mode, solution):
        result = []
        lines = []
        with open(solution, 'r') as fh:
            lines = fh.readlines()
        for line in lines:
            if line.startswith("Project(\""):
                remaining = eval("(" + line[len("Project(\"{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}\") = "):].strip() + ")")
                if remaining[1].startswith("src"):
                    path = os.path.join(os.curdir, remaining[1])
                    dirname = os.path.dirname(path)
                    dllName = os.path.basename(dirname)
                    j = os.path.join
                    path = j(j(dirname, "bin"), mode)
                    result.append(path)
        return result
    
