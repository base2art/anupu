class Task:
  
    def __init__(self, logger, params = None, dependencies = None):
        if "_logger" in dir(logger):
            logger = logger._logger
        self._logger = logger;
        if (params == None) ^ (dependencies == None):
            raise Exception("Invalid Operation: use the correct types that are not Null")
        self._params = params;
        self._dependencies = dependencies;
    
    def invoke(self, params = {}, dependencies = None):
        if self._dependencies != None: dependencies = self._dependencies;
        if self._params != None: params = self._params;
        
        self._logger.Log("Invoking %s with params: %s"% (type(self), params))
        if "_invokeInternal" in dir(self):
            req = self.requiredParameters()
            for item in req:
                if item not in params:
                    self._logger.LogError("MissingRequiredParams: Error Invoking %s with params: %s requires: %s"% (type(self), params, req))
                    return False;
            hasTooMany = False
            for item in params.keys():
                if item not in self.optionalParameters():
                    hasTooMany = True
            if hasTooMany:
                self._logger.Log("TooManyParams: Error Invoking %s with params: %s"% (type(self), params))
            dependencies = self._dependantTasks();
            for item in dependencies:
                result = item.invoke(params)
                if not result:
                    return False
            result = self._invokeInternal(params)
            return self.isSuccessResult(result)
            
        elif dependencies != None:
            for item in dependencies:
                result = item.invoke(params)
                if not result:
                    return False
            return True
        else:
            self._logger.LogError("Error Invoking %s with params: %s"% (type(self), params))
            return False
    
    def requiredParameters(self):
        if "_requiredParameters" in dir(self):
            return self._requiredParameters()
        return [];
    
    def optionalParameters(self):
        optParams = []
        if "_optionalParameters" in dir(self):
            optParams = self._optionalParameters()
        return optParams + self.requiredParameters();
    
    def description(self):
        self._logger.Log("Descripting Task %s"% (type(self)))
        return ""
    
    def _dependantTasks(self):
        return []
    
    def isSuccessResult(self, result):
        return (0 == result or result == None);
    
