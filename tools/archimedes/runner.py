import sys
class Usage:
    def __init__(self):
        self._params = {}
        args = sys.argv[1:]
        for arg in args:
            if arg.startswith("/") and ":" in arg:
                index = arg.index(":")
                self._params[arg[1: index]] = arg[index+1:]
    def GetBuildMode(self):
        if "mode" in self._params: return self._params["mode"]
        return "Release"
    def GetPublishDirectoryName(self):
        mode = self.GetBuildMode();
        if mode == "Debug": return "binn"
        if mode == "Release": return "binn"
        return "bino"
    
        
