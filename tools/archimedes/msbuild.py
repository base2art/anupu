import subprocess, os, shutil, time
from task import Task
from clean import Clean
from msbuildutil import SLN

msbuildExe = "c:\\Windows\\Microsoft.NET\\Framework\\v4.0.30319\\MSBuild.exe"
class MSBuild(Task):
    def __init__(self, logger):
        Task.__init__(self, logger)
        
    def _invokeInternal(self, params):
        mode = params["mode"]
        solution = params["solution"]
        
        
        p = subprocess.Popen([msbuildExe, "/property:Configuration=%s" % mode, solution], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (out, err) = p.communicate()
        retCode = p.returncode
        if retCode != 0:
            trip = "\"\"\""
            lines = eval(trip + str(out) + trip).splitlines()
            errors = []
            for line in lines:
                if 'error' in line.strip():
                    errors.append(line.strip())
            self._logger.LogError (("Build Failed: %s in %s mode.\n\t" %(solution, mode)) + "\n".join(errors).strip() )
        else:
            self._logger.Log ("Build Passed: %s in %s mode." %(solution, mode))
            params["test_dlls"] = SLN.GetTestDLLs(mode, solution)
            params["production_dll_sources"] = SLN.GetProductionBinDirectories(mode, solution)
        return retCode
    def _requiredParameters(self):
        return ["mode", "solution"]
    def _dependantTasks(self):
        return [Clean(self)]
