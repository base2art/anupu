import subprocess, os, shutil, time
from task import Task
from clean import Clean

class NUnit(Task):
    def __init__(self, logger):
        Task.__init__(self, logger)
        
    def _invokeInternal(self, params):
        nunit_console = params["nunit_console"]
        test_dlls = params["test_dlls"]
        solution = params["solution"]
        if len(test_dlls) == 0: return None
            
        
        p = subprocess.Popen([nunit_console, test_dlls], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (out, err) = p.communicate()
        retCode = p.returncode
        if retCode != 0:
            trip = "\"\"\""
            lines = eval(trip + " " + str(out) + " " + trip).splitlines()
            self._logger.Log (lines)
            errors = []
            for line in lines:
                if 'error' in line.strip():
                    errors.append(line.strip())
            self._logger.LogError (("Tests Failed: %s.\n\t" %(solution)) + "\n".join(errors).strip() )
        else:
            self._logger.Log ("Tests Passed: %s" %(solution))
        return retCode
    def _requiredParameters(self):
        return ["nunit_console", "solution", "test_dlls"]
