import subprocess, os, shutil, time
from task import Task

class XmlUpgrade(Task):
    def __init__(self, logger):
        Task.__init__(self,logger)
        
    def _invokeInternal(self, params):
        xmlupgrade_path = params["xmlupgrade_path"]
        xmlupgrade_file = params["xmlupgrade_file"]
        
        
        p = subprocess.Popen([xmlupgrade_path, "/property:path=%s" % xmlupgrade_file], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (out, err) = p.communicate()
        retCode = p.returncode
        if retCode != 0:
            trip = "\"\"\""
            lines = eval(trip + str(out) + trip).splitlines()
            errors = []
            for line in lines:
                errors.append(line.strip())
            self._logger.LogError ("XmlUpgrade Failed: " + "\n".join(errors).strip() )
        else:
            self._logger.Log ("XmlUpgrade Passed." )
        return retCode
    
        
    def _requiredParameters(self):
        return ["xmlupgrade_path", "xmlupgrade_file"]
    

    
