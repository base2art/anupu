import subprocess, os, shutil, time, zipfile
from task import Task
from msbuildutil import SLN

class CreateBuildScript(Task):
    def __init__(self, logger):
        Task.__init__(self, logger)

    def _invokeInternal(self, params):
        contents = """
$currDir = [Environment]::CurrentDirectory
cd "$currDir"
$pythonPath = (Get-Item "hklm:\\SOFTWARE\\Python\\PythonCore\\3.0\\InstallPath").getValue("") + "python.exe"
invoke-expression '& $pythonPath deploy-locally.py'
exit
"""
        if not( os.path.isfile("build.ps1")):
            with open("build.ps1", "w") as fh:
                fh.write(contents)
            pass
        pass
    
        


class DeployArchimedes(Task):
    def __init__(self, logger):
        Task.__init__(self,logger)
        self.Archimedes = "Archimedes"
        
    def _invokeInternal(self, params):
        parentArchimedes = "..\\" + self.Archimedes 
        if os.path.exists(parentArchimedes):
            if(os.path.exists(self.Archimedes )):
                shutil.rmtree(self.Archimedes)
            
            shutil.copytree(parentArchimedes,
                            self.Archimedes,
                            ignore=SLN.FileFilter
                            )
        pass
        
    def _requiredParameters(self):
        return []
    

   

class ZipIt(Task):
    def __init__(self, logger):
        Task.__init__(self,logger)

    
    def _toZip(self, zipper, directory):
        zippedHelp = zipfile.ZipFile(zipper, "w", compression=zipfile.ZIP_DEFLATED )
        self._addFoldersToZip(zippedHelp, directory, directory)
        zippedHelp.close()
     
    def _addFoldersToZip(self, zippedHelp, directory, baseDir):
        dirs = os.listdir(directory)
        for entity in dirs:
            each = os.path.join(directory, entity)
            if os.path.isfile(each):
                arcName = "output/" + each[len(baseDir):]
                zippedHelp.write(each, arcName, zipfile.ZIP_DEFLATED)
            else:
                self._addFoldersToZip(zippedHelp, each, baseDir)

        
    def _invokeInternal(self, params):
        baseDir = "..\\ZipItFolder"
        outputFile = "output.zip"
        if os.path.isfile(outputFile):
            os.remove(outputFile)
        self._tempDir = baseDir
        counter = 0
        while os.path.exists(self._tempDir):
            self._tempDir = baseDir + str(counter)
            counter+=1
        #os.mkdir(self._tempDir)
        shutil.copytree(".", self._tempDir, ignore=SLN.FileFilter)
        #print(self._tempDir + ".zip" + ":" + self._tempDir)
        self._toZip(self._tempDir + ".zip", self._tempDir)
        shutil.move(self._tempDir + ".zip", outputFile)
        shutil.rmtree(self._tempDir)
        pass
        
    def _requiredParameters(self):
        return []
    
