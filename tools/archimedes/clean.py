import subprocess, os, shutil, time
from task import Task
from msbuildutil import SLN
class Clean(Task):
        
    def __init__(self, task):
        Task.__init__(self, task)       
    def _invokeInternal(self, params):
        mode = params["mode"]
        solution = params["solution"]
        self._deleteItemsFromBuildBin(SLN.GetProductionBinDirectories(mode, solution))
    
    def _deleteItemsFromBuildBin(self, paths):
        for path in paths:
            if os.path.isdir(path):
                shutil.rmtree(path)
            
    def _requiredParameters(self):
        return ["mode", "solution"]
    def successResult(self): return None;


class MSClean(Clean):
        
    def __init__(self, task):
        Clean.__init__(self, task)       

    def _deleteItemsFromBuildBin(self, paths):
        for path in paths:
            if os.path.isdir(path):
                for item in os.listdir(path):
                    if item.endswith(".dll") or item.endswith(".pdb"):
                        os.remove(os.path.join(path, item));
