﻿namespace Base2art.Security.Authentication
{
	using System;
	using System.Net.Mail;
    using Base2art.Cryptography.Algorithms;
	using FluentAssertions;
	using Moq;
	using NUnit.Framework;

	[TestFixture]
	public class AuthenticationProcessorTriggerResetPasswordFeature : AuthTestBase
	{
		[Test]
		public void ShouldProcess_EmailNotParse()
		{
			this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>())).Returns(() => null);
			this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>())).Returns(() => null);
			var result = this.processor.TriggerResetPassword(this.UserData());
			result.Should().Be(ResetPasswordStatus.UserNotFound);
		}

		[TestCase(true, ResetPasswordStatus.Success)]
		[TestCase(false, ResetPasswordStatus.UserNotFound)]
		public void ShouldProcess(bool found, ResetPasswordStatus status)
		{
			var user = found ? this.User(CryptoAlgorithmSchema.PlainText) : null;
			this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>())).Returns(user);
			this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>())).Returns(user);
			if (found)
			{
				this.email.Setup(x => x.SendResetPasswordEmail(It.IsAny<string>(), It.IsAny<MailAddress>()));
				//				this.authentication.Setup(x => x(It.IsAny<string>()));
				this.authentication.Setup(x => x.UpdatePassword(It.IsAny<string>(), It.IsAny<string>()));
			}
			var result = this.processor.TriggerResetPassword(this.UserData());
			result.Should().Be(status);
			this.email.VerifyAll();
		}
	}
}









