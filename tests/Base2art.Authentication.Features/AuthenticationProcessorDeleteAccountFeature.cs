﻿namespace Base2art.Security.Authentication
{
	using System;
	using System.Net.Mail;
    using Base2art.Cryptography.Algorithms;
    using Base2art.Security.Authentication;
	using FluentAssertions;
	using Moq;
	using NUnit.Framework;

	[TestFixture]
	public class AuthenticationProcessorDeleteAccountFeature : AuthTestBase
	{
		[Test]
		public void ShouldProcess_EmailNotParse()
		{
			this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>())).Returns(() => null);
			this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>())).Returns(() => null);
			var result = this.processor.DeleteAccount(this.UserData());
			result.Should().Be(DeleteAccountStatus.UserNotFound);
		}

		[TestCase(true, DeleteAccountStatus.Success)]
		[TestCase(false, DeleteAccountStatus.UserNotFound)]
		public void ShouldProcess(bool found, DeleteAccountStatus status)
		{
			var user = found ? this.User(CryptoAlgorithmSchema.PlainText) : null;
			this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>())).Returns(user);
			this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>())).Returns(user);
			
			if (found)
			{
				this.email.Setup(x => x.SendDeleteAccountEmail(It.IsAny<string>(), It.IsAny<MailAddress>()));
				this.authentication.Setup(x => x.DeleteAccount(It.IsAny<string>()));
			}
			
			var result = this.processor.DeleteAccount(this.UserData());
			result.Should().Be(status);
			
			this.email.VerifyAll();
		}
	}
}

