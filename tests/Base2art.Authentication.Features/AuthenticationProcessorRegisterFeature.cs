﻿namespace Base2art.Security.Authentication
{
    using System;
    using System.Net.Mail;
    using Base2art.Security.Authentication.Repositories;
    using Base2art.Cryptography.Algorithms;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    
    [TestFixture]
    public class AuthenticationProcessorRegisterFeature : AuthTestBase
    {
        [Test]
        public void ShouldProcess_WithUser_NullUri_UserNotFound_NoEmailOrUserName()
        {
            this.settings.Setup(x => x.MeetsPasswordComplexity(It.IsAny<string>())).Returns(true);
            
            this.email.Setup(x => x.SendCreateUserNameEmail(It.IsAny<string>(), It.IsAny<MailAddress>()));
            this.authentication.Setup(x => x.CreateUser(It.IsAny<string>(), It.IsAny<MailAddress>(), It.IsAny<string>(), It.IsAny<string>(), CryptoAlgorithmSchema.PlainText));
            
            this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>())).Returns(() => null);
            this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>())).Returns(() => null);
            
            this.settings.Setup(x => x.DefaultPasswordSchema).Returns(CryptoAlgorithmSchema.PlainText);
            
            var result = this.processor.Register(this.UserData());
            result.Should().Be(RegisterStatus.Success);
            
            result = this.processor.Register(this.UserData(userName: null));
            result.Should().Be(RegisterStatus.InvalidUserName);
            
            result = this.processor.Register(this.UserData(email: null));
            result.Should().Be(RegisterStatus.InvalidEmail);
            
            result = this.processor.Register(this.UserData(email: "abc.com"));
            result.Should().Be(RegisterStatus.InvalidEmail);
            
            result = this.processor.Register(this.UserData(password: null));
            result.Should().Be(RegisterStatus.InvalidPassword);
        }
        
        [Test]
        public void ShouldProcess_WithUser_ValidateUserDoesntExist()
        {
            this.settings.Setup(x => x.MeetsPasswordComplexity(It.IsAny<string>())).Returns(true);
            
            this.authentication.Setup(x => x.CreateUser(It.IsAny<string>(), It.IsAny<MailAddress>(), It.IsAny<string>(), It.IsAny<string>(), CryptoAlgorithmSchema.PlainText));
            this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>())).Returns(this.User(CryptoAlgorithmSchema.PlainText));
            
            var result = this.processor.Register(this.UserData());
            result.Should().Be(RegisterStatus.DuplicateUser);
        }
        
        [TestCase(false, RegisterStatus.InvalidPasswordComplexity)]
        [TestCase(true, RegisterStatus.Success)]
        public void ShouldProcess_WithUser_ValidatePasswordAgainstSettings(bool meets, RegisterStatus status)
        {
            this.settings.Setup(x => x.MeetsPasswordComplexity(It.IsAny<string>())).Returns(meets);
            
            this.authentication.Setup(x => x.CreateUser(It.IsAny<string>(), It.IsAny<MailAddress>(), It.IsAny<string>(), It.IsAny<string>(), CryptoAlgorithmSchema.PlainText));
            this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>())).Returns(() => null);
            this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>())).Returns(() => null);
            
            this.settings.Setup(x => x.MeetsPasswordComplexity(It.IsAny<string>())).Returns(meets);
            this.settings.Setup(x => x.DefaultPasswordSchema).Returns(CryptoAlgorithmSchema.PlainText);
            
            if (RegisterStatus.Success == status)
            {
                this.email.Setup(x => x.SendCreateUserNameEmail(It.IsAny<string>(), It.IsAny<MailAddress>()));
            }
            
            var result = this.processor.Register(this.UserData());
            result.Should().Be(status);
        }
    }
}

