﻿namespace Base2art.Security.Authentication
{
	using System;
	using FluentAssertions;
	using NUnit.Framework;

	
	[TestFixture]
	public class AuthenticationProcessorCreationFeature : AuthTestBase
	{
        [Test]
        public void ShouldProcess_NullReferences()
        {
            Action mut;
            mut = () => new AuthenticationProcessor(null, this.cryptor.Object, this.email.Object, this.authentication.Object);
            mut.ShouldThrow<ArgumentNullException>();
            
            mut = () => new AuthenticationProcessor(this.settings.Object, null, this.email.Object, this.authentication.Object);
            mut.ShouldThrow<ArgumentNullException>();
            
            mut = () => new AuthenticationProcessor(this.settings.Object, this.cryptor.Object, null, this.authentication.Object);
            mut.ShouldThrow<ArgumentNullException>();
            
            mut = () => new AuthenticationProcessor(this.settings.Object, this.cryptor.Object, this.email.Object, null);
            mut.ShouldThrow<ArgumentNullException>();
        }
	}
}

