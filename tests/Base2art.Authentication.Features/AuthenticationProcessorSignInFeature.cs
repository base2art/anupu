﻿namespace Base2art.Security.Authentication
{
    using System;
    using System.Collections.Generic;
    using System.Net.Mail;
    using System.Security.Cryptography;
    using Base2art.Cryptography.Algorithms;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    
    
    [TestFixture]
    public class AuthenticationProcessorSignInFeature : AuthTestBase
    {
        [Test]
        public void ShouldProcess_NoUsersDefined()
        {
            this.authentication.Setup(x => x.HasUsers()).Returns(false);
            //            this.authorization.Setup(x => x.HasWhiteListEntries).Returns(false);
            
            var result = this.processor.SignIn(this.UserData());
            result.Should().Be(SignInStatus.Success);
        }
        
        [Test]
        public void ShouldProcess_NullUser_NullUri()
        {
            this.authentication.Setup(x => x.HasUsers()).Returns(true);
            //            this.authorization.Setup(x => x.HasWhiteListEntries).Returns(false);
            var result = this.processor.SignIn(null);
            result.Should().Be(SignInStatus.UnknownUser);
        }
        
        [Test]
        public void ShouldProcess_WithUser_NullUri_UserNotFound()
        {
            this.authentication.Setup(x => x.HasUsers()).Returns(true);
            this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>())).Returns(() => null);
            //            this.authorization.Setup(x => x.HasWhiteListEntries).Returns(false);
            
            this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>())).Returns(() => null);
            
            var result = this.processor.SignIn(this.UserData());
            result.Should().Be(SignInStatus.UnknownUser);
        }
        
        [Test]
        public void ShouldProcess_WithUser_NullUri_UserNotFound_NoUserName()
        {
            this.authentication.Setup(x => x.HasUsers()).Returns(true);
            this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>())).Returns(() => null);
            //            this.authorization.Setup(x => x.HasWhiteListEntries).Returns(false);
            
            var result = this.processor.SignIn(this.UserData(userName:null));
            result.Should().Be(SignInStatus.UnknownUser);
        }
        
        [Test]
        public void ShouldProcess_WithUser_NullUri_UserNotFound_NoUserName_InvalidEmail()
        {
            this.authentication.Setup(x => x.HasUsers()).Returns(true);
            this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>())).Returns(() => null);
            //            this.authorization.Setup(x => x.HasWhiteListEntries).Returns(false);
            
            var result = this.processor.SignIn(this.UserData(userName:null, email:"abc"));
            result.Should().Be(SignInStatus.UnknownUser);
        }
        
        [Test]
        public void ShouldProcess_WithUser_NullUri_UserNotFound_NoEmailOrUserName()
        {
            this.authentication.Setup(x => x.HasUsers()).Returns(true);
            this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>())).Returns(() => null);
            //            this.authorization.Setup(x => x.HasWhiteListEntries).Returns(false);
            
            var result = this.processor.SignIn(this.UserData(userName:null, email:null));
            result.Should().Be(SignInStatus.UnknownUser);
        }
        
        [Test]
        public void ShouldProcess_HasWhiteList()
        {
            this.authentication.Setup(x => x.HasUsers()).Returns(true);
            this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>())).Returns(() => null);
            this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>())).Returns(() => null);
            //            this.authorization.Setup(x => x.HasWhiteListEntries).Returns(true);
            //            this.authorization.Setup(x => x.HasWhiteListEntryByUri(null)).Returns(false);
            
            var result = this.processor.SignIn(this.UserData());
            result.Should().Be(SignInStatus.UnknownUser);
        }
        
        [Test]
        public void ShouldProcess_UserLockedOut()
        {
            var mock = this.UserMock(CryptoAlgorithmSchema.PlainText);
            mock.Setup(x => x.MaxInvalidAttempts).Returns(5);
            mock.Setup(x => x.CurrentInvalidAttempts).Returns(5);
            mock.Setup(x => x.LastInvalidAttempt).Returns(DateTimeOffset.Now.AddMinutes(-29));
            
            this.authentication.Setup(x => x.HasUsers()).Returns(true);
            //            this.authorization.Setup(x => x.HasWhiteListEntries).Returns(false);
            this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>()))
                .Returns(() => mock.Object);
            
            var result = this.processor.SignIn(this.UserData());
            result.Should().Be(SignInStatus.LockedOut);
        }
        
        [Test]
        public void ShouldProcess_UserLockedOut_OverTimeLine()
        {
            var mock = this.UserMock(CryptoAlgorithmSchema.PlainText);
            mock.Setup(x => x.MaxInvalidAttempts).Returns(5);
            mock.Setup(x => x.CurrentInvalidAttempts).Returns(5);
            mock.Setup(x => x.LastInvalidAttempt).Returns(DateTimeOffset.Now.AddMinutes(-31));
            
            this.authentication.Setup(x => x.ResetInvalidAttemptCount(It.IsAny<string>()));
            
            this.authentication.Setup(x => x.HasUsers()).Returns(true);
            this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>()))
                .Returns(() => mock.Object);
            
            var result = this.processor.SignIn(this.UserData());
            result.Should().Be(SignInStatus.Success);
            
            this.authentication.VerifyAll();
        }
        
        [TestCase("AbC", "AbC", SignInStatus.Success)]
        [TestCase("AbC", "AbCD", SignInStatus.BadPassword)]
        public void ShouldProcess_WithUser_NullUri_UserFoundByName_Hash(string inputPassword, string userPassword, SignInStatus status)
        {
            CryptoAlgorithmSchema userPasswordSchema  = CryptoAlgorithmSchema.PlainText;
            
            this.authentication.Setup(x => x.HasUsers()).Returns(true);
            
            this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>()))
                .Returns(() => this.User(userPasswordSchema, userPassword));
            
            if (status != SignInStatus.Success)
            {
                this.authentication.Setup(x => x.IncreaseInvalidAttemptCount(It.IsAny<string>()));
            }
            
            var result = this.processor.SignIn(this.UserData(inputPassword));
            result.Should().Be(status);
        }
        
        [TestCase("AbC", "AbC", SignInStatus.Success)]
        [TestCase("AbC", "AbCD", SignInStatus.BadPassword)]
        public void ShouldProcess_WithUser_NullUri_UserFoundByName_Enc(string inputPassword, string userPassword, SignInStatus status)
        {
            CryptoAlgorithmSchema userPasswordSchema  = CryptoAlgorithmSchema.Encrypted_AES;
            
            this.authentication.Setup(x => x.HasUsers()).Returns(true);
            this.cryptor.Setup(x => x.Decrypt<AesManaged>(It.IsAny<string>(), It.IsAny<string>())).Returns(userPassword);
            
            this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>()))
                .Returns(() => this.User(userPasswordSchema, userPassword));
            
            if (status != SignInStatus.Success)
            {
                this.authentication.Setup(x => x.IncreaseInvalidAttemptCount(It.IsAny<string>()));
            }
            
            var result = this.processor.SignIn(this.UserData(inputPassword));
            result.Should().Be(status);
        }
        
        /*
        [Test, TestCaseSource("PasswordMatchSchemas")]
        public void ShouldProcess_WithUser_NullUri_UserFoundByName(
            string inputPassword, string userPassword, CryptoAlgorithmSchema userPasswordSchema,
            SignInStatus status)
        {
            this.authentication.Setup(x => x.HasUsers).Returns(true);
            this.cryptor.Setup(x => x.Hash<MD5CryptoServiceProvider>(It.IsAny<string>(), It.IsAny<string>())).Returns("");
            
            //            this.authorization.Setup(x => x.HasWhiteListEntries).Returns(false);
            this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>()))
                .Returns(() => this.User(userPasswordSchema, userPassword));
            
            if (status != SignInStatus.Success)
            {
                this.authentication.Setup(x => x.IncreaseInvalidAttemptCount(It.IsAny<string>()));
            }
            
            var result = this.processor.SignIn(this.UserData(inputPassword));
            result.Should().Be(status);
        }
        
        [Test, TestCaseSource("PasswordMatchSchemas")]
        public void ShouldProcess_WithUser_NullUri_UserFoundByEmail(
            string inputPassword, string userPassword, CryptoAlgorithmSchema userPasswordSchema,
            SignInStatus status)
        {
            this.cryptor.Setup(x => x.Hash<MD5CryptoServiceProvider>(It.IsAny<string>(), It.IsAny<string>()))
                .Returns("hashedValue");
            
            this.authentication.Setup(x => x.HasUsers).Returns(true);
            this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>()))
                .Returns(() => this.User(userPasswordSchema, userPassword));
            
            if (status != SignInStatus.Success)
            {
                this.authentication.Setup(x => x.IncreaseInvalidAttemptCount(It.IsAny<string>()));
            }
            
            var result = this.processor.SignIn(this.UserData(inputPassword, null));
            result.Should().Be(status);
        }
         */
    }
}


/*
 
        
        private string Hash<T>(string passwordPlain, string userSalt)
            where T : HashAlgorithm, new()
        {
            var password = string.Format(
                "{0}::✓::{1}",
                passwordPlain,
                userSalt);
            
            return password.Hash<T>().AsString();
        }
        
        private IUser User(
            PasswordSchema schema,
            string value=StandardPassword,
            string userName="SjY",
            string email="SjY@gmail.com")
        {
            return this.UserMock(schema, value, userName, email).Object;
        }

        private Mock<IUser> UserMock(
            PasswordSchema schema,
            string value=StandardPassword,
            string userName="SjY",
            string email="SjY@gmail.com")
        {
            var map = new Dictionary<PasswordSchema, Func<string, string, string>>
            {
                { PasswordSchema.PlainText, (x, y) => x },
                { PasswordSchema.Encrypted_AES, (x, y) => x.Encrypt<AesManaged>("secret", y).AsString() },
                { PasswordSchema.Encrypted_DES, (x, y) => x.Encrypt<DESCryptoServiceProvider>("secret", y).AsString() },
                { PasswordSchema.Encrypted_RC2, (x, y) => x.Encrypt<RC2CryptoServiceProvider>("secret", y).AsString() },
                { PasswordSchema.Encrypted_Rijndael, (x, y) => x.Encrypt<RijndaelManaged>("secret", y).AsString() },
                { PasswordSchema.Encrypted_TripleDES, (x, y) => x.Encrypt<TripleDESCryptoServiceProvider>("secret", y).AsString() },
                { PasswordSchema.Hashed_MD5, (x, y) => this.Hash<MD5CryptoServiceProvider>(x, y) },
                { PasswordSchema.Hashed_Sha1, (x, y) => this.Hash<SHA1Managed>(x, y) },
                { PasswordSchema.Hashed_Sha256, (x, y) => this.Hash<SHA256Managed>(x, y) },
                { PasswordSchema.Hashed_Sha512, (x, y) => this.Hash<SHA512Managed>(x, y) },
            };
            
            var salt = Guid.NewGuid().ToString("N");
            var password = map.ContainsKey(schema) ? map[schema](value, salt) : value;
            var user = new Mock<IUser>();
            
            user.Setup(x => x.Name).Returns(userName);
            user.Setup(x => x.Email).Returns(new MailAddress(email));
            user.Setup(x => x.Password).Returns(password);
            user.Setup(x => x.PasswordSalt).Returns(salt);
            user.Setup(x => x.PasswordSchema).Returns(schema);
            
            return user;
        }
 
 */
//        [Test]
//        public void ShouldProcess_HasWhiteList_FindsMatch()
//        {
//            this.authentication.Setup(x => x.HasUsers).Returns(true);
//            this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>())).Returns(() => null);
//            this.authorization.Setup(x => x.HasWhiteListEntries).Returns(true);
//            this.authorization.Setup(x => x.HasWhiteListEntryByUri(null)).Returns(true);
//
//            var result = this.processor.SignIn(this.UserData(), null);
//            result.Should().Be(SignInStatus.Success);
//        }