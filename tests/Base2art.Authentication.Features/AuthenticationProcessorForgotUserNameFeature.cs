﻿namespace Base2art.Security.Authentication
{
	using System;
    using System.Net.Mail;
    using Base2art.Cryptography.Algorithms;
	using FluentAssertions;
	using Moq;
	using NUnit.Framework;
	
	[TestFixture]
	public class AuthenticationProcessorForgotUserNameFeature : AuthTestBase
	{
		[Test]
		public void ShouldProcess_ForgotUserName_EmailNotParse()
		{
			var result = this.processor.TriggerForgotUserName("gmail.com");
			result.Should().Be(ForgotUserNameStatus.UserNotFound);
		}
		
		
		[TestCase(true, ForgotUserNameStatus.Success)]
		[TestCase(false, ForgotUserNameStatus.UserNotFound)]
		public void ShouldProcess_ForgotUserName(bool found, ForgotUserNameStatus status)
		{
		    var user = found ? this.User(CryptoAlgorithmSchema.PlainText) : null;
		    this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>())).Returns(user);
		    
		    if (found)
		    {
		        this.email.Setup(x => x.SendForgotUserNameEmail(It.IsAny<string>(), It.IsAny<MailAddress>()));
		    }
		    
			var result = this.processor.TriggerForgotUserName("SjY@gmail.com");
			result.Should().Be(status);
		}
	}
	
	
}



