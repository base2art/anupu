﻿namespace Base2art.Security.Authentication
{
	using System;
	using System.Collections.Generic;
	using System.Net.Mail;
	using System.Security.Cryptography;
	using Repositories;
    using Base2art.Cryptography;
    using Base2art.Cryptography.Algorithms;
	using FluentAssertions;
	using Moq;
	using NUnit.Framework;

	public class AuthTestBase
	{
		protected const string StandardPassword = "Test1234!";

		protected Mock<IAuthenticationUserRepository> authentication;

		protected Mock<ICryptor> cryptor;

		protected Mock<IAuthenticationProcessorSettings> settings;
        
		protected Mock<IAuthenticationEmailService> email;

		protected AuthenticationProcessor processor;

		[SetUp]
		public void BeforeEach()
		{
			this.authentication = new Mock<IAuthenticationUserRepository>(MockBehavior.Strict);
			this.cryptor = new Mock<ICryptor>(MockBehavior.Strict);
			this.settings = new Mock<IAuthenticationProcessorSettings>(MockBehavior.Strict);
			this.email = new Mock<IAuthenticationEmailService>(MockBehavior.Strict);
			this.settings.Setup(x => x.InvalidAttemptSlidingWindow).Returns(TimeSpan.FromMinutes(30));
			this.processor = new AuthenticationProcessor(this.settings.Object, this.cryptor.Object, this.email.Object, authentication.Object);
		}

		protected IUserEntity User(
		    CryptoAlgorithmSchema schema,
		    string value = StandardPassword, 
		    string userName = "SjY",
		    string email = "SjY@gmail.com",
		    Guid? verificationId = null)
		{
			return this.UserMock(schema, value, userName, email, verificationId).Object;
		}

		protected Mock<IUserEntity> UserMock(
		    CryptoAlgorithmSchema schema, 
		    string value = StandardPassword, 
		    string userName = "SjY", 
		    string email = "SjY@gmail.com",
		    Guid? verificationId = null)
		{
			var map = new Dictionary<CryptoAlgorithmSchema, Func<string, string, string>> {
				{
					CryptoAlgorithmSchema.PlainText,
					(x, y) => x
				},
				{
					CryptoAlgorithmSchema.Encrypted_AES,
					(x, y) => x.Encrypt<AesManaged>("secret", y).AsString()
				},
				{
					CryptoAlgorithmSchema.Encrypted_DES,
					(x, y) => x.Encrypt<DESCryptoServiceProvider>("secret", y).AsString()
				},
				{
					CryptoAlgorithmSchema.Encrypted_RC2,
					(x, y) => x.Encrypt<RC2CryptoServiceProvider>("secret", y).AsString()
				},
				{
					CryptoAlgorithmSchema.Encrypted_Rijndael,
					(x, y) => x.Encrypt<RijndaelManaged>("secret", y).AsString()
				},
				{
					CryptoAlgorithmSchema.Encrypted_TripleDES,
					(x, y) => x.Encrypt<TripleDESCryptoServiceProvider>("secret", y).AsString()
				},
				{
					CryptoAlgorithmSchema.Hashed_MD5,
					(x, y) => this.Hash<MD5CryptoServiceProvider>(x, y)
				},
				{
					CryptoAlgorithmSchema.Hashed_Sha1,
					(x, y) => this.Hash<SHA1Managed>(x, y)
				},
				{
					CryptoAlgorithmSchema.Hashed_Sha256,
					(x, y) => this.Hash<SHA256Managed>(x, y)
				},
				{
					CryptoAlgorithmSchema.Hashed_Sha512,
					(x, y) => this.Hash<SHA512Managed>(x, y)
				},
			};
		    
			var salt = Guid.NewGuid().ToString("N");
			var password = map.ContainsKey(schema) ? map[schema](value, salt) : value;
			var user = new Mock<IUserEntity>();
			user.Setup(x => x.Name).Returns(userName);
			user.Setup(x => x.Email).Returns(new MailAddress(email));
			user.Setup(x => x.Password).Returns(password);
			user.Setup(x => x.PasswordSalt).Returns(salt);
			user.Setup(x => x.PasswordSchema).Returns(schema);
			user.Setup(x => x.VerificationId).Returns(verificationId.HasValue ? verificationId.Value : Guid.NewGuid());
			return user;
		}

		protected string Hash<T>(string passwordPlain, string userSalt) where T : HashAlgorithm, new()
		{
			var password = string.Format("{0}::✓::{1}", passwordPlain, userSalt);
			return password.Hash<T>().AsString();
		}

        protected UserData UserData(
            string password=StandardPassword,
            string userName="SjY",
            string email="SjY@gmail.com")
        {
            return new UserData
            {
                Name = userName,
                Email = email,
                Password = password,
            };
        }
	}
}





