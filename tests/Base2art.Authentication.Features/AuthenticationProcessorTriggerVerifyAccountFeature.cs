﻿namespace Base2art.Security.Authentication
{
	using System;
	using System.Net.Mail;
    using Base2art.Cryptography.Algorithms;
	using FluentAssertions;
	using Moq;
	using NUnit.Framework;

	[TestFixture]
	public class AuthenticationProcessorTriggerVerifyAccountFeature : AuthTestBase
	{
		[Test]
		public void ShouldProcess_Verify_EmailNotParse()
		{
			this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>())).Returns(() => null);
			this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>())).Returns(() => null);
			var result = this.processor.TriggerVerifyAccount(this.UserData());
			result.Should().Be(VerifyAccountStatus.UserNotFound);
		}

		[TestCase(true, VerifyAccountStatus.Success)]
		[TestCase(false, VerifyAccountStatus.UserNotFound)]
		public void ShouldProcess(bool found, VerifyAccountStatus status)
		{
			var vid = Guid.NewGuid();
			var user = found ? this.User(CryptoAlgorithmSchema.PlainText, verificationId: vid) : null;
			this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>())).Returns(user);
			
			if (found)
			{
			    this.email.Setup(x => x.SendVerifyAccountEmail(It.IsAny<string>(), It.IsAny<MailAddress>(), It.IsAny<Guid>()));
//				this.authentication.Setup(x => x.MarkUserAsVerified(It.IsAny<string>()));
			}
			else
			{
				this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>())).Returns(user);
			}
			
			var result = this.processor.TriggerVerifyAccount(this.UserData());
			result.Should().Be(status);
			this.email.VerifyAll();
			this.authentication.VerifyAll();
		}
	}
}









