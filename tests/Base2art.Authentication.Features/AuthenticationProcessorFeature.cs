﻿namespace Base2art.Security.Authentication
{
    using System;
    using Base2art.Cryptography;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class TokenManagerFeature
    {
        private Mock<ITokenManagerSettings> managerSettings;
        private Mock<ICryptorSettings> cryptorSettings;

        private TokenManager tokenManager;
        
        [SetUp]
        public void BeforeEach()
        {
            this.managerSettings = new Mock<ITokenManagerSettings>();
            this.cryptorSettings = new Mock<ICryptorSettings>();
            
            
            this.managerSettings.Setup(x => x.TokenSalt).Returns("ABCDEFGHIJK");
            this.cryptorSettings.Setup(x => x.Secret).Returns("This is a temp secret");
            
            this.tokenManager = new TokenManager(this.managerSettings.Object, new Cryptor(this.cryptorSettings.Object));
        }
        
        [Test]
        public void ShouldProcess_HandlesNull()
        {
            
            var item = this.tokenManager.GenerateRawTokenForUser(null, null);
            item.Should().BeNull();
        }

        [Test]
        public void ShouldProcess_Raw_Impersonating()
        {
            var item = this.tokenManager.GenerateRawTokenForUser(new UserTokenData { Name = "Sj>:|Y"}, new UserTokenData {Name = "ABC"});
            item.Should().Be("name:Sj&#62;&#58;&#124;Y>name:ABC");
        }

        [Test]
        public void ShouldProcess_Encrypted_Impersonating()
        {
            var item = this.tokenManager.GenerateEncryptedTokenForUser(new UserTokenData { Name = "Sj>:|Y" }, new UserTokenData { Name = "ABC" });
            var tokens = this.tokenManager.GenerateDecryptedUserToken(item);
            tokens.CurrentUser.Name.Should().Be("Sj>:|Y");
            tokens.ImpersonatedUser.Name.Should().Be("ABC");
        }

        [Test]
        public void ShouldProcess_Raw_NonImpersonating()
        {
            var item = this.tokenManager.GenerateRawTokenForUser(new UserTokenData { Name = "Sj>:|Y"}, null);
            item.Should().Be("name:Sj&#62;&#58;&#124;Y");
        }

        [Test]
        public void ShouldProcess_Encrypted_NonImpersonating()
        {
            var item = this.tokenManager.GenerateEncryptedTokenForUser(new UserTokenData { Name = "Sj>:|Y" }, null);
            var tokens = this.tokenManager.GenerateDecryptedUserToken(item);
            tokens.ImpersonatedUser.Should().BeNull();
        }
    }
}



