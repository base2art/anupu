﻿namespace Base2art.Security.Authentication
{
    using System;
    using System.Net.Mail;
    using Base2art.Cryptography.Algorithms;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class AuthenticationProcessorVerifyAccountFeature : AuthTestBase
    {
        [Test]
        public void ShouldProcess_Verify_EmailNotParse()
        {
            this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>())).Returns(() => null);
            this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>())).Returns(() => null);
            var result = this.processor.VerifyAccount(this.UserData(), Guid.NewGuid());
            result.Should().Be(VerifyAccountStatus.UserNotFound);
        }

        [TestCase(true, true, VerifyAccountStatus.Success)]
        [TestCase(true, false, VerifyAccountStatus.InvalidVerificationId)]
        [TestCase(false, true, VerifyAccountStatus.UserNotFound)]
        [TestCase(false, false, VerifyAccountStatus.UserNotFound)]
        public void ShouldProcess(bool found, bool match, VerifyAccountStatus status)
        {
            var vid = Guid.NewGuid();
            var user = found ? this.User(CryptoAlgorithmSchema.PlainText, verificationId:vid) : null;
            this.authentication.Setup(x => x.FindUserByName(It.IsAny<string>())).Returns(user);
            if (found)
            {
                this.authentication.Setup(x => x.MarkUserAsVerified(It.IsAny<string>()));
            }
            else
            {
                this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>())).Returns(user);
            }
            
            var result = this.processor.VerifyAccount(this.UserData(), match ? vid : Guid.NewGuid());
            result.Should().Be(status);
            
            this.email.VerifyAll();
            this.authentication.VerifyAll();
        }
    }
}







