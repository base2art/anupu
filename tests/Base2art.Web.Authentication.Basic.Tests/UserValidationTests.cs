﻿/* Created By tyoung On 1/16/2010 11:42 PM */
using System;
using System.Configuration;
using FluentAssertions;
using NUnit.Framework;

namespace Base2art.Web.Authentication.Basic.Tests
{
	/// <summary>
	/// Description of ConfigurationTests.
	/// </summary>
	[TestFixture]
	public class UserValidationTests : AuthTestBase
	{
		[Test]
		public void TestLockedOuttedNess()
		{
			Settings.AddUser("syoungblut","poopFace!", 5);
			Settings.Save();
			
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AuthStatus.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AuthStatus.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AuthStatus.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AuthStatus.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AuthStatus.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AuthStatus.LockedOut);
		}
		
		[Test]
		public void ShouldReturnNoUserFound()
		{
			Settings.AddUser("syoungblut","poopFace!", 5);
			Settings.Save();
			
			Settings.AuthenticateUser("syouNgblut2", "poopfac3!").Should().Be(AuthStatus.UnknownUser);
			Settings.AuthenticateUser("syouNgblut2", "poopfac3!").Should().Be(AuthStatus.UnknownUser);
			Settings.AuthenticateUser("syouNgblut2", "poopfac3!").Should().Be(AuthStatus.UnknownUser);
			Settings.AuthenticateUser("syouNgblut2", "poopfac3!").Should().Be(AuthStatus.UnknownUser);
			Settings.AuthenticateUser("syouNgblut2", "poopfac3!").Should().Be(AuthStatus.UnknownUser);
			Settings.AuthenticateUser("syouNgblut2", "poopfac3!").Should().Be(AuthStatus.UnknownUser);
		}
		
		[Test]
		public void TestLockedOuttedNessResets()
		{
			Settings.AddUser("syoungblut","poopFace!", 5);
			Settings.Save();
			
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AuthStatus.BadPassword);

			Settings.AuthenticateUser("syouNgblut", "poopface!").Should().Be(AuthStatus.Success);
			
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AuthStatus.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AuthStatus.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AuthStatus.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AuthStatus.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AuthStatus.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AuthStatus.LockedOut);
		}
		
		[Test]
		public void TestAddSameUser2Times()
		{
		    const string UserName = "User1";
		    const string Password = "password123";
			Settings.AddUser(UserName, Password, 5);
			Settings.Save();
			Settings.AddUser(UserName, Password, 5);
			Settings.Save();
		}

		[Test]
		public void ShouldAuthenticateWhenNoUsersSetUpNoCriteria()
		{
		    Settings.Deactivate();
		    Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AuthStatus.Success);
		    Settings.AuthenticateUser("", "").Should().Be(AuthStatus.Success);
		    Settings.AuthenticateUser("", "").Should().Be(AuthStatus.Success);
		    Settings.AuthenticateUser(null, "").Should().Be(AuthStatus.Success);
		    Settings.AuthenticateUser("", null).Should().Be(AuthStatus.Success);
		    Settings.AuthenticateUser(null, null).Should().Be(AuthStatus.Success);
		}
	}
}
