﻿/* Created By tyoung On 1/16/2010 11:42 PM */


namespace Base2art.Web.Authentication.Basic.Tests
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.Reflection;

    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class DomainExclusionTests : AuthTestBase
    {
        [Test]
        public void ShouldFindExcludedDomains()
        {
            Settings.HasExclusions.Should().BeFalse();
            Settings.AddExclusion("domain:sy.com", "www.scottyoungblut.com");
            Settings.Save();
            Settings.HasExclusions.Should().BeTrue();
            
        }
        
        [Test]
        public void ShouldNotBeAbleToAddDuplicates()
        {   
            Settings.HasExclusions.Should().BeFalse();
            Settings.AddExclusion("domain:scott1.com", "www.ScottYoungblut.com");
            new Action(() => Settings.AddExclusion("domain:scott1.com", "www.sc0ttyoungblut.com")).ShouldThrow<ArgumentException>();
            Settings.AddExclusion("domain:scott2.com", "www.ScottYoungblut.com");
            Settings.Save();
            Settings.HasExclusions.Should().BeTrue();
            
        }
    }
}
