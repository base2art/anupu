﻿
using System;
using System.Configuration;
using System.Linq;

using NUnit.Framework;

namespace Base2art.Web.Authentication.Basic.Tests
{
    public abstract class AuthTestBase : IPathMapper
    {
        private int counter = 0;
        
        private IBasicAuthSettings settings;
        
        protected IBasicAuthSettings Settings
        {
            get { return this.settings; }
        }
        
        protected IBasicAuthSettings GetUsersSection(String name)
        {
            return BasicAuth.CreateUserSettings(this.GetType().Name + "." + name, this);
        }
        
        [SetUp]
        public void TestSetup()
        {
            counter++;
            this.settings = this.GetUsersSection("section" + Environment.TickCount);
        }
    	
		string IPathMapper.MapPath(string pathToMap)
		{
		    string rez = new Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath;
		    
		    return rez;
		}
    }
}
