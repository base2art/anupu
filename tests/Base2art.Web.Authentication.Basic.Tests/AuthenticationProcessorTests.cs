﻿using System.Collections.Specialized;

/* Created By tyoung On 7/30/2010 11:42 PM */


namespace Base2art.Web.Authentication.Basic.Tests
{
    using System;
    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class AuthenticationProcessorTests : AuthTestBase
    {
        protected NameValueCollection CreateHeaders()
        {
            return new NameValueCollection();
        }
        
        protected Uri CreateUri()
        {
            return new UriBuilder(Uri.UriSchemeHttp, "localhost", 80, "/").Uri;
        }
        
        [Test]
        public void ShouldLoadIntegrateSuccessfullyNoUsers()
        {
            
            var processor = new AuthenticationProcessor("Testing", this);
            processor.Settings.Deactivate();
            
            processor.Process(this.CreateHeaders(), this.CreateHeaders(), this.CreateUri()).ShouldBeEquivalentTo(AuthStatus.Success);
        }
        
        [Test]
        public void ShouldLoadIntegrateSuccessfullyUsersCorrectData()
        {
            var processor = new AuthenticationProcessor("Testing", this);
            processor.Settings.AddUser("Aladdin", "open sesame", 1000);
            NameValueCollection inputHeaders = this.CreateHeaders();
            
            inputHeaders.Set("Authorization", "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==");
            
            processor.Process(inputHeaders, this.CreateHeaders(), this.CreateUri()).ShouldBeEquivalentTo(AuthStatus.Success);
        }
        
        [Test]
        public void ShouldLoadIntegrateBadPasswordUsersCorrectData()
        {
            var processor = new AuthenticationProcessor("Testing", this);
            processor.Settings.AddUser("Aladdin", "open sesame", 1000);
            NameValueCollection inputHeaders = this.CreateHeaders();
            
            inputHeaders.Set("Authorization", "Basic QWxhZGRpbjpvcGVuIHNlc3FtZQ==");
            
            processor.Process(inputHeaders, this.CreateHeaders(), this.CreateUri())
                .ShouldBeEquivalentTo(AuthStatus.BadPassword);
        }
        
        [Test]
        public void ShouldLoadIntegrateInvalidRequestMalFormedRequest()
        {
            var runner = new TestRunner(this);
            runner.InputHeaders.Set("Authorization", "Bas ic QWxhZGRpbjpvcGVuIHNlc3FtZQ==");
            runner.Process().ShouldBeEquivalentTo(AuthStatus.InvalidRequest);
        }
        
        [Test]
        public void ShouldLoadIntegrateInvalidRequestWrongMethod()
        {
            var runner = new TestRunner(this);
            runner.InputHeaders.Set("Authorization", "Digest QWxhZGRpbjpvcGVuIHNlc3FtZQ==");
            runner.Process().ShouldBeEquivalentTo(AuthStatus.InvalidRequest);
        }
        
        [Test]
        public void ShouldLoadIntegrateInvalidRequestPasswordWithColons()
        {
            var runner = new TestRunner(this);
            runner.InputHeaders.Set("Authorization", "Basic QWxhZGRpbjpvcGVuIDogc2VzcW1l");
            runner.Process().ShouldBeEquivalentTo(AuthStatus.InvalidRequest);
        }
        
        [Test]
        public void ShouldLoadIntegrateInvalidRequestPasswordWithBadBase64Encoding()
        {
            var runner = new TestRunner(this);
            runner.InputHeaders.Set("Authorization", "Basic QWxhZGRpbjpvcGVuIDogc2V__zcW1l");
            new Action(() => runner.Process()).ShouldThrow<InvalidOperationException>();
        }
        
        [Test]
        public void ShouldLoadIntegrateInvalidRequestPasswordWithEmptyAuth()
        {
            var runner = new TestRunner(this);
            runner.InputHeaders.Set("Authorization", string.Empty);
            runner.Process().Should().Be(AuthStatus.UnknownUser);
        }
        
        [Test]
        public void ShouldLoadIntegrateInvalidRequestPasswordWithNullAuth()
        {
            var runner = new TestRunner(this);
            runner.InputHeaders.Set("Authorization", null);
            runner.Process().Should().Be(AuthStatus.UnknownUser);
        }
        
        [Test]
        public void ShouldFailWhenNoExlcusionsSet()
        {
            var runner = new TestRunner(this, "Test1");
            runner.InputHeaders.Set("Authorization", null);
            runner.Process().Should().Be(AuthStatus.UnknownUser);
        }
        
        [Test]
        public void ShouldValidateWhenNotExcluded()
        {
            var runner = new TestRunner(this, "Test." + Environment.TickCount);
            runner.Uri = new Uri("http://www.scottyoungblut.com/");
            runner.InputHeaders.Set("Authorization", null);
            runner.Process().Should().Be(AuthStatus.UnknownUser);
        }
        
        [Test]
        public void ShouldPassWhenExcluded()
        {
            var runner = new TestRunner(this, "Test." + Environment.TickCount);
            runner.Uri = new Uri("http://www.scottyoungblut.com/");
            runner.InputHeaders.Set("Authorization", null);
            
            runner.Processor.Settings.AddExclusion("Domain:ScottYoungblut", "www.ScottYoungblut.com");
            runner.Process().Should().Be(AuthStatus.Success);
            
            runner.Uri = new Uri("http://scottyoungblut.com/");
            runner.Process().Should().Be(AuthStatus.UnknownUser);
            
            
        }
        
        
        private class TestRunner
        {
            private readonly AuthenticationProcessor processor;
            
            private readonly NameValueCollection inputHeaders = new NameValueCollection();
            private readonly NameValueCollection outputHeaders = new NameValueCollection();
            
            
            public TestRunner(IPathMapper mapper, string sectionName, bool includeAladin = true)
            {
                this.Uri = new UriBuilder(Uri.UriSchemeHttp, "localhost", 80, "/").Uri;
                this.processor = new AuthenticationProcessor(sectionName, mapper);
                if (includeAladin)
                {
                    this.processor.Settings.AddUser("Aladdin", "open sesame", 1000);
                }
            }
            
            public TestRunner(IPathMapper mapper, bool includeAladin = true)
                : this(mapper, "Testing", includeAladin)
            {
            }
            
            public NameValueCollection InputHeaders
            {
                get { return this.inputHeaders; }
            }
            
            public NameValueCollection OutputHeaders
            {
                get { return this.outputHeaders; }
            }
            
            public AuthenticationProcessor Processor
            {
                get { return this.processor; }
            }
            
            public Uri Uri { get; set; }
            
            public AuthStatus Process()
            {
                return this.processor.Process(this.inputHeaders, this.outputHeaders, this.Uri);
            }
        }
    }
}
