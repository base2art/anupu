﻿<%@ Control
    Language           = "C#"
%>
<%@ Import Namespace="Base2art.Security.Authentication" %>
<%@ Import Namespace="Base2art.Security.Characterization" %>
<%@ Import Namespace="Base2art.Security.AccessControl" %>
<%@ Import Namespace="Base2art.Security.Web.Module" %>
<%@ Import Namespace="System.Linq" %>

<%@ Register TagPrefix="b2a" TagName="CommonFunctions" Src="../CommonFuncts.ascx" %>

<b2a:CommonFunctions id="CommonFunctions" runat="server" />

<script runat="server">
    
    
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        
        var security = this.Context.Services().Find<ISecurityService>();
        var roles = Base2art.Security.AccessControl.RoleProvider.GetRoles(
                               security.MembershipProcessor,
                               security.CharacterizationProcessor,
                               CommonFunctions.Value("user", string.Empty));
        this.CurrentItems.DataSource = roles;
        this.CurrentItems.DataBind();
    }
    
    
</script>

<% if (!string.IsNullOrWhiteSpace(CommonFunctions.Value("user", string.Empty))) { %>
    <fieldset style="margin-bottom:60px;">
        <legend> User's roles </legend>
        <ul >
            <asp:Repeater runat="server" id="CurrentItems">
                 <ItemTemplate>
                     <li>
                         <asp:literal runat="server" id="roleName" Text='<%# Container.DataItem%>' Mode="Encode" />
                     </li>
                 </ItemTemplate>
            </asp:Repeater>
        </ul>
    </fieldset>

<% } %>