﻿<%@ Control
    Language           = "C#"
%>
<%@ Import Namespace="Base2art.Security.AccessControl" %>
<%@ Import Namespace="Base2art.Security.Authentication" %>
<%@ Import Namespace="Base2art.Security.Characterization" %>
<%@ Import Namespace="Base2art.Security.Web.Module" %>

<%@ Register TagPrefix="b2a" TagName="CommonFunctions" 
    Src="../CommonFuncts.ascx" %>

<b2a:CommonFunctions id="CommonFunctions" runat="server" />

<script runat="server">

    private IUser user;
    private bool hasChecked;
    
    private readonly System.Collections.Generic.List<string> errors = new System.Collections.Generic.List<string>(); 
    
    public bool IsCreating
    {
        get { return string.IsNullOrWhiteSpace(this.User.Name); }
    }
    
    private IUser User
    {
        get
        {
            if (!this.hasChecked)
            {
                var userName = CommonFunctions.Value("user", string.Empty);
                this.user = CommonFunctions.Security.AuthenticationProcessor.FindUserByName(userName);
                
                if (this.user == null)
                {
                    this.user = new Base2art.Security.Authentication.Repositories.Models.User();
                }
                
                this.hasChecked = true;
            }
            
            return this.user;
        }
    }
    
    protected override void OnInit(System.EventArgs e)
    {
        base.OnInit(e);
        var action = CommonFunctions.Value("action", string.Empty);
        if (action == "Delete_User")
        {
            var userData = new UserData(); 
            userData.Name = CommonFunctions.Value("user", string.Empty);
            var result = CommonFunctions.Security.AuthenticationProcessor.DeleteAccount(userData);
            if (result == DeleteAccountStatus.Success)
            {
                this.Response.Redirect(this.Request.Url.ToString());
            }
            
            this.errors.Add(result.ToString("G"));
            this.hasChecked = false;
        }
        
        if (action == "Create_User")
        {
            var userData = new UserData(); 
            userData.Name = CommonFunctions.Value("__name", string.Empty);
            userData.Email = CommonFunctions.Value("__email", string.Empty);
            userData.Password = CommonFunctions.Value("__password", string.Empty);
            var result = CommonFunctions.Security.AuthenticationProcessor.Register(userData);
            if (result == RegisterStatus.Success)
            {
                this.Response.Redirect(this.Request.Url.ToString());
            }
            
            this.errors.Add(result.ToString("G"));
            this.hasChecked = false;
        }
    }
    
</script>


<form method="POST">
    <fieldset>
        
        <% if (this.IsCreating) { %>
            <legend>Create User</legend>
        <% } else { %>
            <legend>Delete User</legend>
        <% } %>
        
        <%= CommonFunctions.Field_TB("User Name", "__name", required: true, defaultValue: this.User.Name, @readonly: !this.IsCreating) %>
        <%= CommonFunctions.Field_TB("User Email", "__email", required: true, defaultValue: this.User.Email, @readonly: !this.IsCreating) %>
        
        <% if (this.IsCreating) { %>
            <%= CommonFunctions.Field_TB("Password", "__password", required: true) %>
        <% } %>
        
        <div>
            <% if (this.IsCreating) { %>
                <input type="submit" name="action" value="Create_User" />
            <% } else { %>
                <input type="submit" name="action" value="Delete_User" /> 
            <% } %>
            
            <% foreach(var error in this.errors) { %>
                <span style="font-weight:bold; color:Red;"> <%= error %></span>
            <% } %>
            
        </div>
    </fieldset>
</form>
