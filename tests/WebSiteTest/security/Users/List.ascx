﻿<%@ Control
    Language           = "C#"
%>
<%@ Import Namespace="Base2art.Security.Authentication" %>
<%@ Import Namespace="Base2art.Security.AccessControl" %>
<%@ Import Namespace="Base2art.Security.Web.Module" %>
<%@ Import Namespace="Base2art.Collections.Specialized" %>

<%@ Register TagPrefix="b2a" TagName="CommonFunctions" Src="../CommonFuncts.ascx" %>

<b2a:CommonFunctions id="CommonFunctions" runat="server" />

<script runat="server">
    
    private IPagedCollection<IUser> users;
    
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        
        var security = this.Context.Services().Find<ISecurityService>();
        this.users = security.AuthenticationProcessor.Users(new Base2art.Collections.Specialized.Page(0, 20));
        this.CurrentUsers.DataSource = this.users;
        this.CurrentUsers.DataBind();
    }
    
</script>



<table style="width:100%">
    <thead>
        <td>
            User Name
        </td>
        <td>
            Email
        </td>
        <td>
            Max Invalid Attempts
        </td>
        <td>
            Current Invalid Attempts
        </td>
        <td>
            Manage
        </td>
        
    </thead>
    <asp:Repeater runat="server" id="CurrentUsers">
         <ItemTemplate>
             <tr>
                 <td>
                     <asp:literal runat="server" id="username" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>' Mode="Encode" />
                 </td>
                 <td>
                     <asp:literal runat="server" id="email" Text='<%# DataBinder.Eval(Container.DataItem, "Email") %>' Mode="Encode" />
                 </td>
                 <td>
                     <asp:literal runat="server" id="maxInvalid" Text='<%# DataBinder.Eval(Container.DataItem, "MaxInvalidAttempts") %>' Mode="Encode" />
                 </td>
                 <td>
                     <asp:literal runat="server" id="currentInvalid" Text='<%# DataBinder.Eval(Container.DataItem, "CurrentInvalidAttempts") %>' Mode="Encode" />
                 </td>
                 <td>
                     <a href='?tab=users&user=<%# DataBinder.Eval(Container.DataItem, "Name") %>' >Manage</a>
                     <!-- OnClick="HandleManageClick"OnClick="HandleManageClick"-->
                     <%--
                         <asp:Button runat="server" Text="Edit"  CommandName="edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Name") %>' />
                         <asp:Button runat="server" Text="Remove"  CommandName="remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Name") %>' />
                     --%>
                 </td>
             </tr>
         </ItemTemplate>
    </asp:Repeater>
</table>




<div style="float-left; padding:20px;">
    
    <% if ( this.users.Page.HasPreviousPage()) { %> 
        <form method="GET" style="display: inline;">
            <input type="hidden" name="tab" value="users" />
            <input type="hidden" name="group-page" value='<%= CommonFunctions.ValueInt("group-page", 0) - 1 %>' />
            <button style="margin-right:20px;"> &laquo; Previous  </button>
        </form>
    <% } %>
    
    Showing [<%= this.users.Page.Start() + 1 %> - <%= this.users.Page.End() + 1 %>] of <%= this.users.Page.TotalCount %> 
    
    <% if ( this.users.Page.HasNextPage().GetValueOrDefault()) { %> 
        <form method="GET" style="display: inline;">
            <input type="hidden" name="tab" value="users" />
            <input type="hidden" name="group-page" value='<%= CommonFunctions.ValueInt("group-page", 0) + 1 %>' />
            <button style="margin-left:20px;"> Next &raquo; </button>
        </form>
    <% } %>
</div>