﻿<%@ Control
    Language           = "C#"
%>
<%@ Import Namespace="Base2art.Security.AccessControl" %>
<%@ Import Namespace="Base2art.Security.Membership" %>
<%@ Import Namespace="Base2art.Security.Web.Module" %>

<%@ Register TagPrefix="b2a" TagName="CommonFunctions" Src="../CommonFuncts.ascx" %>

<b2a:CommonFunctions id="CommonFunctions" runat="server" />

<script runat="server">

    private IGroup group;
    private bool hasChecked;
    
    private readonly System.Collections.Generic.List<string> errors = new System.Collections.Generic.List<string>(); 
    
    public bool IsCreating
    {
        get { return string.IsNullOrWhiteSpace(this.Group.Name); }
    }
    
    private IGroup Group
    {
        get
        {
            if (!this.hasChecked)
            {
                var groupName = CommonFunctions.Value("group", string.Empty);
                this.group = CommonFunctions.Security.MembershipProcessor.FindGroupByName(groupName);
                
                if (this.group == null)
                {
                    this.group = new Base2art.Security.Membership.Repositories.Models.Group();
                }
                
                this.hasChecked = true;
            }
            
            return this.group;
        }
    }
    
    protected override void OnInit(System.EventArgs e)
    {
        base.OnInit(e);
        var action = CommonFunctions.Value("action", string.Empty);
        
        if (action == "Update_Group")
        {
            var groupData = new GroupData(); 
            groupData.Name = CommonFunctions.Value("__name", string.Empty);
            groupData.Description = CommonFunctions.Value("__description", string.Empty);
            var result = CommonFunctions.Security.MembershipProcessor.UpdateGroup(groupData);
            if (result == GroupCreationStatus.Success)
            {
                this.Response.Redirect(this.Request.Url.ToString());
            }
            
            this.errors.Add(result.ToString("G"));
            this.hasChecked = false;
        }
        
        if (action == "Delete_Group")
        { 
            var groupName = CommonFunctions.Value("group", string.Empty);
            var result = CommonFunctions.Security.MembershipProcessor.RemoveGroup(groupName);
            if (result == GroupRemovalStatus.Success)
            {
                this.Response.Redirect(this.Request.Url.ToString());
            }
            
            this.errors.Add(result.ToString("G"));
            this.hasChecked = false;
        }
        
        if (action == "Create_Group")
        {
            var groupData = new GroupData(); 
            groupData.Name = CommonFunctions.Value("__name", string.Empty);
            groupData.Description = CommonFunctions.Value("__description", string.Empty);
            var result = CommonFunctions.Security.MembershipProcessor.CreateGroup(groupData);
            if (result == GroupCreationStatus.Success)
            {
                this.Response.Redirect(this.Request.Url.ToString());
            }
            
            this.errors.Add(result.ToString("G"));
            this.hasChecked = false;
        }
    }
    
</script>

<form method="POST">
    <fieldset>
        
        <% if (this.IsCreating) { %>
            <legend>Create Group</legend>
        <% } else { %>
            <legend>Delete Group</legend>
        <% } %>
        
        <%= CommonFunctions.Field_TB("Name", "__name", required: true, defaultValue: this.Group.Name, @readonly: !this.IsCreating) %>
        <%= CommonFunctions.Field_TextArea("Description", "__description", required: true, defaultValue: this.Group.Description) %>
        
        
        <div>
            <% if (this.IsCreating) { %>
                <input type="submit" name="action" value="Create_Group" />
            <% } else { %>
                <input type="submit" name="action" value="Update_Group" />
                &nbsp;
                <input type="submit" name="action" value="Delete_Group" /> 
            <% } %>
            
            <% foreach(var error in this.errors) { %>
                <span style="font-weight:bold; color:Red;"> <%= error %></span>
            <% } %>
            
        </div>
    </fieldset>
</form>
