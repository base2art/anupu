﻿<%@ Control
    Language           = "C#"
%>
<%@ Import Namespace="Base2art.Security.Authentication" %>
<%@ Import Namespace="Base2art.Security.Membership" %>
<%@ Import Namespace="Base2art.Security.AccessControl" %>
<%@ Import Namespace="Base2art.Security.Web.Module" %>
<%@ Import Namespace="System.Linq" %>

<%@ Register TagPrefix="b2a" TagName="CommonFunctions" Src="../CommonFuncts.ascx" %>

<b2a:CommonFunctions id="CommonFunctions" runat="server" />

<script runat="server">
    
    private bool hasChecked;
    
    private readonly System.Collections.Generic.List<string> errors = new System.Collections.Generic.List<string>(); 
    
    protected override void OnInit(System.EventArgs e)
    {
        base.OnInit(e);
        var action = CommonFunctions.Value("action", string.Empty);
        
        if (action == "remove_GroupGroup")
        {
            var child_group = CommonFunctions.Value("child_group", string.Empty);
            var group = CommonFunctions.Value("group", string.Empty);
            var result = CommonFunctions.Security.MembershipProcessor.RemoveGroupFromGroup(group, child_group);
            if (result == GroupAssignmentStatus.Success)
            {
                this.Response.Redirect(this.Request.Url.ToString());
            }
            
            this.errors.Add(result.ToString("G"));
            this.hasChecked = false;
        }
        
        if (action == "assign_GroupGroup")
        {
            var group = CommonFunctions.Value("group", string.Empty);
            var child_group = CommonFunctions.Value("child_group", string.Empty);
            var result = CommonFunctions.Security.MembershipProcessor.AssignGroupToGroup(group, child_group);
            if (result == GroupAssignmentStatus.Success)
            {
                this.Response.Redirect(this.Request.Url.ToString());
            }
            
            this.errors.Add(result.ToString("G"));
            this.hasChecked = false;
        }
    }
    
    
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        
        var security = this.Context.Services().Find<ISecurityService>();
        var group = security.MembershipProcessor.FindGroupByName(CommonFunctions.Value("group", string.Empty));
        this.CurrentItems.DataSource = group.MemberGroups;
        this.CurrentItems.DataBind();
    }
    
    private IEnumerable<string> ChildGroups()
    {
        var security = this.Context.Services().Find<ISecurityService>();
        
        var group = security.MembershipProcessor.FindGroupByName(CommonFunctions.Value("group", string.Empty));
        var childGroups = group.MemberGroups;
        
        var allGroups = security.MembershipProcessor.Groups(new Base2art.Collections.Specialized.Page(0, 10000));
        return allGroups.Select(x=>x.Name).Where(x => !childGroups.Any(y=> y == x));
    }
    
</script>

<% foreach(var error in this.errors) { %>
    <span style="font-weight:bold; color:Red;"> <%= error %></span>
<% } %>

<fieldset style="margin-bottom:60px;">
    <legend>Add Group to Group</legend>
    <ul >
        <asp:Repeater runat="server" id="CurrentItems">
             <ItemTemplate>
                 <li>
                     <asp:literal runat="server" id="roleName" Text='<%# Container.DataItem%>' Mode="Encode" />
                     <form method="POST" style="display: inline;">
                           <input type="hidden" name="action" value="remove_GroupGroup" />
                           <input type="hidden" name="child_group" value="<%# Container.DataItem%>" />
                           <button style="margin-left:20px;"> Remove </button>
                     </form>
                 </li>
             </ItemTemplate>
        </asp:Repeater>
    </ul>
</fieldset>

<form method="POST">

    <fieldset>
        <legend>Add Group to Role</legend>
        <input type="hidden" name="action" value="assign_GroupGroup" />
        <select name="child_group">
            <option value="">Choose Group...</option>
            
            <% foreach (var item in this.ChildGroups()) { %>
                <option><%= item %></option>
            <% } %>
        </select>
        
        <button style="margin-left:20px;"> Add </button>
    </fieldset>
</form>
