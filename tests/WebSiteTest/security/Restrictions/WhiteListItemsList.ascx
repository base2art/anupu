﻿<%@ Control
    Language           = "C#"
%>
<%@ Import Namespace="Base2art.Security.Authorization" %>
<%@ Import Namespace="Base2art.Security.AccessControl" %>
<%@ Import Namespace="Base2art.Security.Web.Module" %>

<%@ Register TagPrefix="b2a" TagName="CommonFunctions" Src="../CommonFuncts.ascx" %>

<b2a:CommonFunctions id="CommonFunctions" runat="server" />

<script runat="server">
    
    
    private readonly System.Collections.Generic.List<string> errors = new System.Collections.Generic.List<string>(); 
    
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
        var action = CommonFunctions.Value("action", string.Empty);
        
        if (action == "remove_white_list_item")
        {
            var permission = new UriMapFilterData();
            permission.CaseSensitive = CommonFunctions.ValueBool("__case_sensitive", false);
            permission.Host = CommonFunctions.Value("__host", string.Empty);
            permission.Port = CommonFunctions.ValueIntQ("__port", null);
            permission.Path = CommonFunctions.Value("__path", string.Empty);
            
            var result = CommonFunctions.Security.AuthorizationProcessor.RemoveWhiteListPermission(permission);
            if (result == RemovePermissionStatus.Success_OneMatch || result == RemovePermissionStatus.Success_ManyMatches)
            {
                this.Response.Redirect(this.Request.Url.ToString());
            }
            
            this.errors.Add(result.ToString("G"));
        }
        
        if (action == "create_white_list_item")
        {
            var permission = new UriMapFilterData();
            permission.CaseSensitive = CommonFunctions.ValueBool("__case_sensitive", false);
            permission.Host = CommonFunctions.Value("__host", string.Empty);
            permission.Port = CommonFunctions.ValueIntQ("__port", null);
            permission.Path = CommonFunctions.Value("__path", string.Empty);
            
            var result = CommonFunctions.Security.AuthorizationProcessor.AddWhiteListPermission(permission);
            if (result == CreatePermissionStatus.Success)
            {
                this.Response.Redirect(this.Request.Url.ToString());
            }
            
            this.errors.Add(result.ToString("G"));
        }
    }
    
    
    
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        
        var security = this.Context.Services().Find<ISecurityService>();
        var users = security.AuthorizationProcessor.WhiteListPermissions(new Base2art.Collections.Specialized.Page(0, 20));
        this.CurrentItems.DataSource = users;
        this.CurrentItems.DataBind();
    }
    
</script>


<% foreach(var error in this.errors) { %>
    <span style="font-weight:bold; color:Red;"> <%= error %></span>
<% } %>


<table style="width:100%; margin-bottom:30px;"  border="1">
    <thead>
        <td>
            Host
        </td>
        <td>
            Path
        </td>
        <td>
            Case Sensitive
        </td>
        <td>
            Port
        </td>
        <td>
            Manage
        </td>
        
    </thead>
    <asp:Repeater runat="server" id="CurrentItems">
         <ItemTemplate>
             <tr>
                 <td>
                     <asp:literal runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Host") %>' Mode="Encode" />
                 </td>
                 <td>
                     <asp:literal runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Path") %>' Mode="Encode" />
                 </td>
                 <td>
                     <asp:literal runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CaseSensitive") %>' Mode="Encode" />
                 </td>
                 <td>
                     <asp:literal runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Port") %>' Mode="Encode" />
                 </td>
                 <td style="width:1px;">
                     <form method="POST" style="display: inline;">
                           <input type="hidden" name="action" value="remove_white_list_item" />
                           <input type="hidden" name="__host" value='<%# DataBinder.Eval(Container.DataItem, "Host") %>' />
                           <input type="hidden" name="__path" value='<%# DataBinder.Eval(Container.DataItem, "Path") %>' />
                           <input type="hidden" name="__port" value='<%# DataBinder.Eval(Container.DataItem, "Port") %>' />
                           <input type="hidden" name="__case_sensitive" value='<%# DataBinder.Eval(Container.DataItem, "CaseSensitive") %>' />
                           <button> Remove </button>
                     </form>
                 </td>
             </tr>
         </ItemTemplate>
    </asp:Repeater>
</table>



<form method="POST">
    <fieldset>
        
        <legend>Create WhiteList Permission</legend>
        
        <%= CommonFunctions.Field_TB("Host", "__host") %>
        <%= CommonFunctions.Field_TB("Path", "__path") %>
        <%= CommonFunctions.Field_TB("Port", "__port") %>
        <%= CommonFunctions.Field_CB("Path Is Case Sensitive", "__case_sensitive") %>
        
        <div>
            <input type="submit" name="action" value="create_white_list_item" />
            
            <% foreach(var error in this.errors) { %>
                <span style="font-weight:bold; color:Red;"> <%= error %></span>
            <% } %>
            
        </div>
    </fieldset>
</form>