﻿<%@ Control
    Language           = "C#"
%>
<%@ Import Namespace="Base2art.Security.AccessControl" %>
<%@ Import Namespace="Base2art.Security.Web.Module" %>

<%@ Register TagPrefix="b2a" TagName="CommonFunctions" Src="../CommonFuncts.ascx" %>

<b2a:CommonFunctions id="CommonFunctions" runat="server" />

<script runat="server">
    
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        
        var security = this.Context.Services().Find<ISecurityService>();
        var roles = security.CharacterizationProcessor.Roles(new Base2art.Collections.Specialized.Page(0, 20));
        this.CurrentRoles.DataSource = roles;
        this.CurrentRoles.DataBind();
    }
    
</script>



<table style="width:100%">
    <thead>
        <td>
            Role Name
        </td>
        <td>
            Role Description
        </td>
        <td>
            Manage
        </td>
        <td>
            Users
        </td>
        <td>
            Groups
        </td>
        
    </thead>
    <asp:Repeater runat="server" id="CurrentRoles">
         <ItemTemplate>
             <tr>
                 <td>
                     <asp:literal runat="server" id="roleName" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>' Mode="Encode" />
                 </td>
                 <td>
                     <asp:literal runat="server" id="description" Text='<%# CommonFunctions.Truncate(75, DataBinder.Eval(Container.DataItem, "Description")) %>' Mode="Encode" />
                 </td>
                 <td>
                     <a href='?tab=roles&role=<%# DataBinder.Eval(Container.DataItem, "Name") %>' >Manage</a>
                 </td>
                 <td>
                     <a href='?tab=roles_users&role=<%# DataBinder.Eval(Container.DataItem, "Name") %>' > Edit (<%# DataBinder.Eval(Container.DataItem, "Users.Length") %>) </a>
                 </td>
                 <td>
                     <a href='?tab=roles_groups&role=<%# DataBinder.Eval(Container.DataItem, "Name") %>' > Edit (<%# DataBinder.Eval(Container.DataItem, "Groups.Length") %>) </a>
                 </td>
             </tr>
         </ItemTemplate>
    </asp:Repeater>
</table>
