﻿namespace Base2art.Security.Authentication.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using Base2art.Collections.Specialized;
    using Base2art.Cryptography.Algorithms;
    using Models;
    
    public class JsonAuthenticationUserRepository : IAuthenticationUserRepository
    {
        private readonly string userFilePath;
        
        private KeyedCollection<string, User> users = new MyKeyedCollection();

        public JsonAuthenticationUserRepository(string userFilePath)
        {
            this.userFilePath = userFilePath;
            this.ResetUsers();
        }

        protected IUserEntity[] Users
        {
            get { return this.users.ToArray<IUserEntity>(); }
        }
        
        public bool HasUsers()
        {
            return this.users.Count > 0;
        }

        IPagedCollection<IUserEntity> IAuthenticationUserRepository.Users(Page page)
        {
            var currentUsers = this.users;
            return new PagedCollection<IUserEntity>(
                currentUsers.OrderBy(x => x.Name).Skip(page.Start()).Take(page.PageSize),
                page,
                currentUsers.Count);
        }
        
        IUserEntity IAuthenticationUserRepository.FindUserByName(string name)
        {
            return this.FindUserByName(name);
        }
        
        public IUserEntity FindUserByEmail(System.Net.Mail.MailAddress email)
        {
            return this.Users.FirstOrDefault(x => string.Equals(x.Email.Address, email.Address));
        }
        
        public void IncreaseInvalidAttemptCount(string name)
        {
            var user = this.FindUserByName(name);
            if (user == null)
            {
                return;
            }
            
            user.CurrentInvalidAttempts += 1;
            this.StoreUser(user);
        }
        
        public void ResetInvalidAttemptCount(string name)
        {
            var user = this.FindUserByName(name);
            if (user == null)
            {
                return;
            }
            
            user.CurrentInvalidAttempts = 0;
            this.StoreUser(user);
        }
        
        public void MarkUserAsVerified(string name)
        {
            var user = this.FindUserByName(name);
            if (user == null)
            {
                return;
            }
            
            user.EmailVerified = true;
            this.StoreUser(user);
        }
        
        public void CreateUser(string name, System.Net.Mail.MailAddress address, string password, string passwordSalt, CryptoAlgorithmSchema schema)
        {
            var user = this.FindUserByName(name);
            if (user != null)
            {
                return;
            }
            
            var newUser = new User
            {
                Name = name,
                Email = address.ToString(),
                Password = password,
                PasswordSalt = passwordSalt,
                PasswordSchema = schema,
                MaxInvalidAttempts = 0,
                VerificationId = Guid.NewGuid(),
            };
            
            this.StoreUser(newUser);
        }
        
        public void UpdatePassword(string name, string password)
        {
            var user = this.FindUserByName(name);
            if (user == null)
            {
                return;
            }
            
            user.Password = password;
            this.StoreUser(user);
        }
        
        public void DeleteAccount(string name)
        {
            var user = this.FindUserByName(name);
            if (user == null)
            {
                return;
            }
            
            this.DeleteUser(user);
        }
        
        protected User FindUserByName(string name)
        {
            try
            {
                return this.users[name];
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void ResetUsers()
        {
            if (!File.Exists(this.userFilePath))
            {
                File.WriteAllText(this.userFilePath, "[]");
            }
            
            var file = File.ReadAllText(this.userFilePath);
            var serializier = new Serialization.NewtonsoftSerializer();
            var currentUsers = serializier.Deserialize<List<User>>(file);
            var collection = new MyKeyedCollection();
            
            foreach (var current in currentUsers)
            {
                collection.Add(current);
            }
            
            this.users = collection;
        }
        
        private void DeleteUser(User user)
        {
            var file = File.ReadAllText(this.userFilePath);
            var serializier = new Serialization.NewtonsoftSerializer();
            var currentUsers = serializier.Deserialize<List<User>>(file);
            currentUsers.RemoveAll(x => string.Equals(x.Name, user.Name, StringComparison.OrdinalIgnoreCase));
            
            var output = serializier.Serialize<List<User>>(currentUsers);
            File.WriteAllText(this.userFilePath, output);
            this.ResetUsers();
        }
        
        private void StoreUser(User user)
        {
            var file = File.ReadAllText(this.userFilePath);
            var serializier = new Serialization.NewtonsoftSerializer();
            var currentUsers = serializier.Deserialize<List<User>>(file);
            
            currentUsers.RemoveAll(x => string.Equals(x.Name, user.Name, StringComparison.OrdinalIgnoreCase));
            currentUsers.Add(user);
            
            var output = serializier.Serialize<List<User>>(currentUsers);
            File.WriteAllText(this.userFilePath, output);
            
            this.ResetUsers();
        }
        
        private class MyKeyedCollection : KeyedCollection<string, User>
        {
            protected override string GetKeyForItem(User item)
            {
                return item.Name;
            }
        }
    }
}
