﻿namespace Base2art.Security.Authentication.Repositories
{
    using System;
    using System.Net.Mail;
    using Base2art.Cryptography.Algorithms;

    public interface IAuthenticationUserRepository
    {
        bool HasUsers();

        Base2art.Collections.Specialized.IPagedCollection<IUserEntity> Users(Base2art.Collections.Specialized.Page page);

        IUserEntity FindUserByName(string name);

        IUserEntity FindUserByEmail(MailAddress email);
        
        void IncreaseInvalidAttemptCount(string name);

        void ResetInvalidAttemptCount(string name);

        void MarkUserAsVerified(string name);

        void CreateUser(string name, MailAddress address, string password, string passwordSalt, CryptoAlgorithmSchema schema);

        void UpdatePassword(string name, string password);

        void DeleteAccount(string name);
    }
    
}
