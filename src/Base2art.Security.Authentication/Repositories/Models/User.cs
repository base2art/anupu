﻿namespace Base2art.Security.Authentication.Repositories.Models
{
    using System;
    using System.Linq;
    using System.Net.Mail;
    using Base2art.Cryptography.Algorithms;

    public class User : IUserEntity
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public CryptoAlgorithmSchema PasswordSchema { get; set; }

        public string PasswordSalt { get; set; }

        public int MaxInvalidAttempts { get; set; }

        public int CurrentInvalidAttempts { get; set; }

        public DateTimeOffset LastInvalidAttempt { get; set; }

        public Guid VerificationId { get; set; }

        public bool EmailVerified { get; set; }
        
        MailAddress IUser.Email
        {
            get
            {
                try
                {
                    return new MailAddress(this.Email);
                }
                catch (Exception) 
                {
                    return null;
                }
            }
        }
    }
}


