﻿namespace Base2art.Security.Authentication
{
    public class UserData
    {
        /// <summary>
        /// The User's Name, which is a unique Identifier
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// The User's email, which is a unique Identifier
        /// </summary>
        public string Email { get; set; }
        
        /// <summary>
        /// The User's Password
        /// </summary>
        public string Password { get; set; }
    }
}
