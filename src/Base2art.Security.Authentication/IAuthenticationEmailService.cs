﻿namespace Base2art.Security.Authentication
{
    using System;
    using System.Net.Mail;
    
    public interface IAuthenticationEmailService
    {
        void SendForgotUserNameEmail(string name, MailAddress email);

        void SendCreateUserNameEmail(string name, MailAddress email);

        void SendResetPasswordEmail(string name, MailAddress email);

        void SendVerifyAccountEmail(string name, MailAddress email, Guid verificationId);

        void SendDeleteAccountEmail(string name, MailAddress mailAddress);
    }
}
