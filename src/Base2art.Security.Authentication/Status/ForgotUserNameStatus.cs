﻿/* Created By tyoung On 1/17/2010 1:00 AM */

namespace Base2art.Security.Authentication
{
    public enum ForgotUserNameStatus
    {
        /// <summary>
        /// Allow Account creation
        /// </summary>
        Success = 0,
        
        /// <summary>
        /// User Not Found
        /// </summary>
        UserNotFound = 1
    }
}
