﻿/* Created By tyoung On 1/17/2010 1:00 AM */

namespace Base2art.Security.Authentication
{
    public enum ResetPasswordStatus
    {
        /// <summary>
        /// Allow Account creation
        /// </summary>
        Success = 0,
        
        /// <summary>
        /// User Not Found
        /// </summary>
        UserNotFound,
        
        /// <summary>
        /// The Password is invalid.
        /// </summary>
        InvalidPassword,
        
        /// <summary>
        /// The password is not complex enough.
        /// </summary>
        InvalidPasswordComplexity
    }
}
