﻿/* Created By tyoung On 1/17/2010 1:00 AM */

namespace Base2art.Security.Authentication
{
    public enum RegisterStatus
    {
        /// <summary>
        /// Allow Account creation
        /// </summary>
        Success = 0,
        
        /// <summary>
        /// Invalid User Name
        /// </summary>
        InvalidUserName = 1,
        
        /// <summary>
        /// Invalid User Name
        /// </summary>
        InvalidEmail = 2,
        
        /// <summary>
        /// Invalid Password
        /// </summary>
        InvalidPassword = 3,
        
        /// <summary>
        /// Invalid Password
        /// </summary>
        InvalidPasswordComplexity = 4,
        
        /// <summary>
        /// Invalid Password
        /// </summary>
        DuplicateUser = 5,
        
        /// <summary>
        /// Unknown Encryption Type or other error
        /// </summary>
        InvalidRequest = 6
    }
}
