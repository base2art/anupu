﻿/* Created By tyoung On 1/17/2010 1:00 AM */

namespace Base2art.Security.Authentication
{
    /// <summary>
    /// Reasons For a access being denied.
    /// </summary>
    public enum SignInStatus
    {
        /// <summary>
        /// Allow Access
        /// </summary>
        Success = 0,
        
        /// <summary>
        /// To many invalid password attempts
        /// </summary>
        LockedOut = 1,
        
        /// <summary>
        /// The current failure is the result of a bad password, for a valid user
        /// </summary>
        BadPassword = 2,
        
        /// <summary>
        /// The current failure is the result of a user not being in the system.
        /// </summary>
        UnknownUser = 3,
        
        /// <summary>
        /// The current failure is the result of a malformed request or using the wrong protocol.
        /// </summary>
        InvalidRequest = 4
    }
}
