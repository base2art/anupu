﻿namespace Base2art.Security.Authentication
{
    using System;
    using Base2art.Cryptography.Algorithms;
    
    public interface IAuthenticationProcessorSettings
    {
        TimeSpan InvalidAttemptSlidingWindow { get; }

//        string Secret { get; }
        
        CryptoAlgorithmSchema DefaultPasswordSchema { get; }

        bool MeetsPasswordComplexity(string password);
    }
}
