﻿namespace Base2art.Security.Authentication
{
    public interface IUserToken
    {
        string Name { get; }
    }
}
