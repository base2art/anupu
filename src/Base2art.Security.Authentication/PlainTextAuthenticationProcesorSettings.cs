﻿namespace Base2art.Security.Authentication
{
    using System;
    using System.Linq;
    using Base2art.Cryptography.Algorithms;
    
    public class PlainTextAuthenticationProcesorSettings : IAuthenticationProcessorSettings
    {
        public System.TimeSpan InvalidAttemptSlidingWindow
        {
            get { return TimeSpan.FromMinutes(15); }
        }

        public string Secret
        {
            get { return string.Empty; }
        }

        public CryptoAlgorithmSchema DefaultPasswordSchema
        {
            get { return CryptoAlgorithmSchema.PlainText; }
        }
        
        public bool MeetsPasswordComplexity(string password)
        {
            if (password == null)
            {
                return false;
            }
            
            password = password.Trim();
            var digits = password.Count(x => char.IsDigit(x));
            var uppers = password.Count(x => char.IsLetter(x) && char.IsUpper(x));
            
            var punct  = password.Count(x => !char.IsLetterOrDigit(x));
            
            if (password.Length >= 6 && (digits > 0 || uppers > 0 || punct > 0))
            {
                return true;
            }
            
            return false;
        }
    }
}
