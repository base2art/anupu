﻿namespace Base2art.Security.Authentication
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base2art.Cryptography;
    using Base2art.Cryptography.Algorithms;
    
    public class TokenManager : ITokenManager
    {
        private const char TokenSeparator    = '>';
        private const char PropertySeparator = '|';
        private const char KVPSeprator       = ':';
        
        private const string NameKey = "name";
        
        private static readonly Dictionary<char, string> Mapper = new Dictionary<char, string>
        {
            { TokenSeparator, "&#62;" },
            { PropertySeparator, "&#124;" },
            { KVPSeprator, "&#58;" },
        };
        
        private readonly ITokenManagerSettings settings;

        private readonly ICryptor cryptor;
        
        public TokenManager(ITokenManagerSettings settings, ICryptor cryptor)
        {
            if (cryptor == null)
            {
                throw new ArgumentNullException("cryptor");
            }
            
            if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }
            
            this.settings = settings;
            this.cryptor = cryptor;
        }
        
        public IUserTokens GenerateDecryptedUserToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                return null;
            }
            
            var algorithm = CryptoAlgorithmSchema.Encrypted_Rijndael.GetAlgorithm();
            var encToken = algorithm.Decode(this.cryptor, token, this.settings.TokenSalt);
            
            if (string.IsNullOrWhiteSpace(encToken))
            {
                return null;
            }
            
            if (encToken.Contains(TokenSeparator))
            {
                var tokens = encToken.Split(TokenSeparator);
                var token1 = tokens[0];
                var token2 = tokens[1];
                return new UserTokens(this.TokenStringToUserToken(token1), this.TokenStringToUserToken(token2));
            }
            
            return new UserTokens(this.TokenStringToUserToken(encToken));
        }

        public string GenerateEncryptedTokenForUser(UserTokenData currentUser, UserTokenData userImpersonating)
        {
            var rawToken = this.GenerateRawTokenForUser(currentUser, userImpersonating);
            var algorithm = CryptoAlgorithmSchema.Encrypted_Rijndael.GetAlgorithm();
            var encToken = algorithm.Encode(this.cryptor, rawToken, this.settings.TokenSalt);
            return encToken;
        }

        public string GenerateRawTokenForUser(UserTokenData currentUser, UserTokenData userImpersonating)
        {
            if (currentUser == null)
            {
                return null;
            }
            
            Func<KeyValuePair<string, string>, string> mapString = pair =>
            {
                var key = this.EncodeItem(pair.Key);
                var value = this.EncodeItem(pair.Value ?? string.Empty);
                
                return string.Concat(key, KVPSeprator, value);
            };
            
            Func<UserTokenData, string> mapUser = user =>
            {
                var pairs = new Dictionary<string, string>
                {
                    {
                        "name",
                        user.Name
                    }
                };
                
                var cleanPairStrings = pairs.Select(mapString);
                return string.Join(new string(PropertySeparator, 1), cleanPairStrings);
            };
            
            if (userImpersonating == null)
            {
                return mapUser(currentUser);
            }
            
            var tokenPlainText = string.Concat(mapUser(currentUser), TokenSeparator, mapUser(userImpersonating));
            return tokenPlainText;
        }

        protected virtual string EncodeItem(string item)
        {
            foreach (var chp in Mapper)
            {
                item = item.Replace(new string(chp.Key, 1), chp.Value);
            }
            
            return item;
        }

        protected virtual string DecodeItem(string item)
        {
            foreach (var chp in Mapper)
            {
                item = item.Replace(chp.Value, new string(chp.Key, 1));
            }
            
            return item;
        }

        private IUserToken TokenStringToUserToken(string token)
        {
            var items = token.Split(PropertySeparator);
            var userToken = new UserToken();
            foreach (var item in items)
            {
                var pair = item.Split(KVPSeprator);
                if (pair.Length != 2)
                {
                    return null;
                }
                
                var key = pair[0];
                var value = pair[1];
                
                if (string.Equals(key, NameKey))
                {
                    userToken.Name = this.DecodeItem(value);
                }
            }
            
            return userToken;
        }
    }
}
