﻿namespace Base2art.Security.Authentication
{
    public interface ITokenManager
    {
        IUserTokens GenerateDecryptedUserToken(string token);

        string GenerateEncryptedTokenForUser(UserTokenData currentUser, UserTokenData userImpersonating);
    }
}
