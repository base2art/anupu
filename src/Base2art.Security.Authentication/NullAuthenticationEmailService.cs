﻿namespace Base2art.Security.Authentication
{
    using System;
    using System.Net.Mail;

    public sealed class NullAuthenticationEmailService : IAuthenticationEmailService
    {
        void IAuthenticationEmailService.SendForgotUserNameEmail(string name, MailAddress email)
        {
        }
        
        void IAuthenticationEmailService.SendCreateUserNameEmail(string name, MailAddress email)
        {
        }
        
        void IAuthenticationEmailService.SendResetPasswordEmail(string name, MailAddress email)
        {
        }
        
        void IAuthenticationEmailService.SendVerifyAccountEmail(string name, MailAddress email, Guid verificationId)
        {
        }
        
        void IAuthenticationEmailService.SendDeleteAccountEmail(string name, MailAddress mailAddress)
        {
        }
    }
}
