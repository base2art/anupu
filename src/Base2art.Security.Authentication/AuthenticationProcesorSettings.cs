﻿namespace Base2art.Security.Authentication
{
    using System;
    using System.Linq;
    using Base2art.Cryptography.Algorithms;

    public class AuthenticationProcessorSettings : IAuthenticationProcessorSettings
    {
        private readonly TimeSpan invalidAttemptSlidingWindow;

        private readonly CryptoAlgorithmSchema schema;
        
        public AuthenticationProcessorSettings(TimeSpan invalidAttemptSlidingWindow, CryptoAlgorithmSchema schema)
        {
            this.schema = schema;
            this.invalidAttemptSlidingWindow = invalidAttemptSlidingWindow;
        }

        public System.TimeSpan InvalidAttemptSlidingWindow
        {
            get { return this.invalidAttemptSlidingWindow; }
        }

        public CryptoAlgorithmSchema DefaultPasswordSchema
        {
            get { return this.schema; }
        }
        
        public bool MeetsPasswordComplexity(string password)
        {
            if (password == null)
            {
                return false;
            }
            
            password = password.Trim();
            var digits = password.Count(x => char.IsDigit(x));
            var uppers = password.Count(x => char.IsLetter(x) && char.IsUpper(x));
            
            var punct = password.Count(x => !char.IsLetterOrDigit(x));
            if (password.Length >= 6 && (digits > 0 || uppers > 0 || punct > 0))
            {
                return true;
            }
            
            return false;
        }
    }
}
