﻿namespace Base2art.Security.Authentication
{
    using System;
    using System.Linq;
    using System.Net.Mail;
    using Base2art.Security.Authentication.Repositories;
    using Base2art.Cryptography;
    using Base2art.Cryptography.Algorithms;

    public class AuthenticationProcessor : IAuthenticationProcesssor
    {
        private readonly IAuthenticationProcessorSettings settings;
        
        private readonly IAuthenticationUserRepository userRepo;

        private readonly ICryptor cryptor;

        private readonly IAuthenticationEmailService emailService;

        public AuthenticationProcessor(
            IAuthenticationProcessorSettings settings,
            ICryptor cryptor,
            IAuthenticationEmailService emailService,
            IAuthenticationUserRepository userRepo)
        {
            if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }
            
            if (cryptor == null)
            {
                throw new ArgumentNullException("cryptor");
            }
            
            if (userRepo == null)
            {
                throw new ArgumentNullException("userRepo");
            }
            
            if (emailService == null)
            {
                throw new ArgumentNullException("emailService");
            }
            
            this.userRepo = userRepo;
            this.emailService = emailService;
            this.cryptor = cryptor;
            this.settings = settings;
        }

        public bool HasUsers()
        {
            return this.userRepo.HasUsers();
        }

        public IUser FindUserByName(string userName)
        {
            return this.userRepo.FindUserByName(userName);
        }
        
        public Base2art.Collections.Specialized.IPagedCollection<IUser> Users(Base2art.Collections.Specialized.Page page)
        {
            return this.userRepo.Users(page);
        }
        
        public RegisterStatus Register(UserData userData)
        {
            if (string.IsNullOrWhiteSpace(userData.Name))
            {
                return RegisterStatus.InvalidUserName;
            }
            
            if (string.IsNullOrWhiteSpace(userData.Email))
            {
                return RegisterStatus.InvalidEmail;
            }
            
            MailAddress mailAddress = ParseEmail(userData.Email);
            if (mailAddress == null)
            {
                return RegisterStatus.InvalidEmail;
            }
            
            var user = this.FindUser(userData);
            if (user != null)
            {
                return RegisterStatus.DuplicateUser;
            }
            
            var result = this.VerifyPassword(
                userData.Password,
                RegisterStatus.InvalidPassword,
                RegisterStatus.InvalidPasswordComplexity);
            
            if (result.HasValue)
            {
                return result.Value;
            }
            
            var salt = Guid.NewGuid().ToString("N");
            
            var schema = this.settings.DefaultPasswordSchema;
            var userDataPassword = this.GeneratePassword(userData, salt, schema);
            
            if (string.IsNullOrWhiteSpace(userDataPassword))
            {
                return RegisterStatus.InvalidRequest;
            }
            
            this.userRepo.CreateUser(
                userData.Name,
                mailAddress,
                userDataPassword,
                salt,
                schema);
            
            this.emailService.SendCreateUserNameEmail(userData.Name, mailAddress);
            
            return RegisterStatus.Success;
        }
        
        public SignInStatus SignIn(UserData userData)
        {
            if (!this.userRepo.HasUsers())
            {
                return SignInStatus.Success;
            }
            
            var user = this.FindUser(userData);
            
            if (user == null)
            {
                return SignInStatus.UnknownUser;
            }
            
            if (user.MaxInvalidAttempts > 0 && user.CurrentInvalidAttempts >= user.MaxInvalidAttempts)
            {
                if (user.LastInvalidAttempt > DateTimeOffset.UtcNow.Subtract(this.settings.InvalidAttemptSlidingWindow))
                {
                    return SignInStatus.LockedOut;
                }
                else
                {
                    this.userRepo.ResetInvalidAttemptCount(user.Name);
                }
            }
            
            if (string.IsNullOrWhiteSpace(userData.Password))
            {
                return SignInStatus.BadPassword;
            }
            
            CryptoAlgorithmSchema schema = user.PasswordSchema;
            
            if (schema.IsHash())
            {
                var userPassword = this.cryptor.Hash(userData, user);
                if (string.Equals(user.Password, userPassword))
                {
                    return SignInStatus.Success;
                }
            }
            
            if (schema.IsEncryptor())
            {
                var userPassword = this.cryptor.Decrypt(userData, user);
                if (string.Equals(userData.Password, userPassword))
                {
                    return SignInStatus.Success;
                }
            }
            
            this.userRepo.IncreaseInvalidAttemptCount(user.Name);
            return SignInStatus.BadPassword;
        }
        
        public ForgotUserNameStatus TriggerForgotUserName(string emailAddress)
        {
            var userData = new UserData { Email = emailAddress };
            
            return this.PerformActionOnUser(
                userData,
                ForgotUserNameStatus.UserNotFound,
                ForgotUserNameStatus.Success,
                user => this.emailService.SendForgotUserNameEmail(user.Name, user.Email));
        }
        
        public ResetPasswordStatus TriggerResetPassword(UserData userData)
        {
            return this.PerformActionOnUser(
                userData,
                ResetPasswordStatus.UserNotFound,
                ResetPasswordStatus.Success,
                user => this.emailService.SendResetPasswordEmail(user.Name, user.Email));
        }
        
        public ResetPasswordStatus ResetPassword(UserData userData)
        {
            var result = this.VerifyPassword<ResetPasswordStatus>(
                userData.Password,
                ResetPasswordStatus.InvalidPassword,
                ResetPasswordStatus.InvalidPasswordComplexity);
            
            if (result.HasValue)
            {
                return result.Value;
            }
            
            return this.PerformActionOnUser(
                userData,
                ResetPasswordStatus.UserNotFound,
                ResetPasswordStatus.Success,
                user => this.userRepo.UpdatePassword(
                    user.Name,
                    this.GeneratePassword(userData, user.PasswordSalt, user.PasswordSchema)));
        }
        
        public VerifyAccountStatus TriggerVerifyAccount(UserData userData)
        {
            return this.PerformActionOnUser(
                userData,
                VerifyAccountStatus.UserNotFound,
                VerifyAccountStatus.Success,
                user => this.emailService.SendVerifyAccountEmail(user.Name, user.Email, user.VerificationId));
        }
        
        public VerifyAccountStatus VerifyAccount(UserData userData, Guid userVerificationId)
        {
            return this.PerformActionOnUser(
                userData,
                VerifyAccountStatus.UserNotFound,
                VerifyAccountStatus.Success,
                user => this.userRepo.MarkUserAsVerified(user.Name),
                user => userVerificationId != user.VerificationId,
                VerifyAccountStatus.InvalidVerificationId);
        }

        public DeleteAccountStatus DeleteAccount(UserData userData)
        {
            return this.PerformActionOnUser(
                userData,
                DeleteAccountStatus.UserNotFound,
                DeleteAccountStatus.Success,
                user =>
                {
                    this.userRepo.DeleteAccount(userData.Name);
                    this.emailService.SendDeleteAccountEmail(user.Name, user.Email);
                });
        }

        private T PerformActionOnUser<T>(
            UserData userData,
            T userNotFound,
            T success,
            Action<IUserEntity> action,
            Func<IUserEntity, bool> additionalCheck = null,
            T? additionalCheckResult = null)
            where T : struct
        {
            var user = this.FindUser(userData);
            if (user == null)
            {
                return userNotFound;
            }
            
            if (action != null)
            {
                action(user);
            }
            
            if (additionalCheck != null)
            {
                if (additionalCheck(user))
                {
                    return additionalCheckResult.Value;
                }
            }
            
            return success;
        }
        
        private IUserEntity FindUser(UserData userData)
        {
            if (userData == null)
            {
                return null;
            }
            
            IUserEntity user;
            if (!string.IsNullOrWhiteSpace(userData.Name))
            {
                user = this.userRepo.FindUserByName(userData.Name);
                if (user != null)
                {
                    return user;
                }
            }
            
            if (!string.IsNullOrWhiteSpace(userData.Email))
            {
                var email = ParseEmail(userData.Email);
                if (email == null)
                {
                    return null;
                }
                
                return this.userRepo.FindUserByEmail(email);
            }
            
            return null;
        }

        private T? VerifyPassword<T>(string password, T invalidItem, T doesnMeetComplexity)
            where T : struct
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                return invalidItem;
            }
            
            if (!this.settings.MeetsPasswordComplexity(password.Trim()))
            {
                return doesnMeetComplexity;
            }
            
            return null;
        }
        
        private string GeneratePassword(UserData userData, string salt, CryptoAlgorithmSchema schema)
        {
            var userDataPassword = userData.Password.Trim();
            var algorithm = CryptoAlgorithms.GetAlgorithm(schema);
            if (algorithm != null)
            {
                userDataPassword = algorithm.Encode(this.cryptor, userDataPassword, salt);
            }
            
            return userDataPassword;
        }

        private static MailAddress ParseEmail(string email)
        {
            try
            {
                return new MailAddress(email);
            }
            catch (FormatException)
            {
                return null;
            }
        }
    }
}
