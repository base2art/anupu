﻿namespace Base2art.Security.Authentication
{
    public interface IUserTokens
    {
        IUserToken ImpersonatedUser { get; set; }
        
        IUserToken CurrentUser { get; set; }
    }
}
