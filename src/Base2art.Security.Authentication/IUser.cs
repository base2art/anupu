﻿/* Created By tyoung On 1/16/2010 11:14 PM */

namespace Base2art.Security.Authentication
{
    using System;
    using System.Net.Mail;
    using Base2art.Cryptography.Algorithms;


    /// <summary>
    /// The contract that represents information about a user.
    /// </summary>
    public interface IUser
    {
        /// <summary>
        /// The User's Name, which is a unique Identifier
        /// </summary>
        string Name { get; }
        
        /// <summary>
        /// The User's Email, which is a unique Identifier
        /// </summary>
        MailAddress Email { get; }
        
        /// <summary>
        /// The User's Password
        /// </summary>
        CryptoAlgorithmSchema PasswordSchema { get; }
        
        /// <summary>
        /// The Maximum number of invalid attempts allowed with this user.
        /// </summary>
        int MaxInvalidAttempts { get; }
        
        /// <summary>
        /// The current Number of invalid attempts
        /// </summary>
        int CurrentInvalidAttempts { get; }
        
        /// <summary>
        /// The The last instance an invalid attempt
        /// </summary>
        DateTimeOffset LastInvalidAttempt { get; }
        
        /// <summary>
        /// Gets an value indicating whether or not the user's email has been verified.  
        /// </summary>
        bool EmailVerified { get; }
    }
}