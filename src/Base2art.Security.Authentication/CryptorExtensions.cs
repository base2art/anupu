﻿namespace Base2art.Security.Authentication
{
    using System;
    using Base2art.Cryptography;
    using Base2art.Cryptography.Algorithms;

    public static class CryptorExtensions
    {
        public static string Hash(this ICryptor cryptor, UserData userData, IUserEntity user)
        {
            return user.PasswordSchema.GetAlgorithm()
                .Encode(cryptor, userData.Password, user.PasswordSalt);
        }
        
        public static string Decrypt(this ICryptor cryptor, UserData userData, IUserEntity user)
        {
            return user.PasswordSchema.GetAlgorithm()
                .Decode(cryptor, userData.Password, user.PasswordSalt);
        }
    }
}
