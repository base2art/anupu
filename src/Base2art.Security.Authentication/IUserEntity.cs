﻿/* Created By tyoung On 1/16/2010 11:14 PM */

namespace Base2art.Security.Authentication
{
	using System;
	using System.Net.Mail;
	using Base2art.Cryptography.Algorithms;

	/// <summary>
	/// The contract that represents information about a user.
	/// </summary>
	public interface IUserEntity : IUser
	{
		/// <summary>
		/// The User's Password
		/// </summary>
		string Password { get; }

		/// <summary>
		/// The salt used with the users information
		/// </summary>
		string PasswordSalt { get; }

        /// <summary>
        /// The id Used for verification.
        /// </summary>
        Guid VerificationId { get; }
	}
}

