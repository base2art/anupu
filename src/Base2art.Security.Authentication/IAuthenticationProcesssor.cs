﻿namespace Base2art.Security.Authentication
{
    using System;
    using Base2art.Collections.Specialized;
    
    public interface IAuthenticationProcesssor
    {
        bool HasUsers();
        
        IPagedCollection<IUser> Users(Page page);

        IUser FindUserByName(string userName);
        
        RegisterStatus Register(UserData userData);

        SignInStatus SignIn(UserData userData);

        ForgotUserNameStatus TriggerForgotUserName(string emailAddress);

        ResetPasswordStatus TriggerResetPassword(UserData userData);

        ResetPasswordStatus ResetPassword(UserData userData);

        VerifyAccountStatus TriggerVerifyAccount(UserData userData);

        VerifyAccountStatus VerifyAccount(UserData userData, Guid userVerificationId);

        DeleteAccountStatus DeleteAccount(UserData userData);
    }
}
