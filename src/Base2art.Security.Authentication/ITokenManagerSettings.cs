﻿namespace Base2art.Security.Authentication
{
    public interface ITokenManagerSettings
    {
        string TokenSalt { get; }
    }
}
