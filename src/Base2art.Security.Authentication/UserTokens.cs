﻿namespace Base2art.Security.Authentication
{
    public class UserTokens : IUserTokens
    {
        public UserTokens(IUserToken userToken)
        {
            this.CurrentUser = userToken;
        }
        
        public UserTokens(IUserToken currentUser, IUserToken impersonatedUser)
        {
            this.CurrentUser = currentUser;
            this.ImpersonatedUser = impersonatedUser;
        }

        public IUserToken ImpersonatedUser { get; set; }
        
        public IUserToken CurrentUser { get; set; }
    }
}
