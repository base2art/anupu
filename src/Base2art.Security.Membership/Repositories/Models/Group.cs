﻿namespace Base2art.Security.Membership.Repositories.Models
{
    public class Group : IGroup
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string[] MemberGroups { get; set; }
        public string[] MemberUsers { get; set; }
    }
}
