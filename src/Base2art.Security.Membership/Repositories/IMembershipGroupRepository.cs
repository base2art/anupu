﻿namespace Base2art.Security.Membership.Repositories
{
    using Base2art.Collections.Specialized;
    
	public interface IMembershipGroupRepository
	{
	    string[] UserGroups(string userName);

        void RemoveGroupFromGroup(string containerGroupName, string groupName);
        
        void RemoveUserFromGroup(string groupName, string userName);

        IGroup FindGroup(string groupName);

        IPagedCollection<IGroup> Groups(Page page);
        
        void AddGroup(string groupName, string description);

        void UpdateGroupByName(string groupName, string description);
        
        void AssignUserToGroup(string groupName, string userName);

        void AssignGroupToGroup(string containerGroupName, string groupName);

        void RemoveGroup(string groupName);
	}
	
}


