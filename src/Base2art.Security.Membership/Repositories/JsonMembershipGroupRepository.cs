﻿namespace Base2art.Security.Membership.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using Base2art.Collections.Specialized;
    using Repositories.Models;
    
    public class JsonMembershipGroupRepository : IMembershipGroupRepository
    {
        private readonly string groupFilePath;
        
        private KeyedCollection<string, Group> groups = new MyKeyedCollection();

        public JsonMembershipGroupRepository(string groupFilePath)
        {
            this.groupFilePath = groupFilePath;
            this.ResetGroups();
        }
        
        public bool HasGroups
        {
            get { return this.groups.Count > 0; }
        }
        
        public IPagedCollection<IGroup> Groups(Page page)
        {
            var currentGroups = this.groups;
            return new PagedCollection<IGroup>(
                currentGroups.OrderBy(x => x.Name).Skip(page.Start()).Take(page.PageSize),
                page,
                currentGroups.Count);
        }
        
        public string[] UserGroups(string userName)
        {
            var currentGroups = this.groups;
            var parentGroups = new HashSet<string>(currentGroups
                                                  .Where(group => ChildUsers(group).Contains(userName))
                                                  .Select(x => x.Name)
                                                  .ToArray(), StringComparer.OrdinalIgnoreCase);

            var foundNewGroup = parentGroups.Count > 0;
            
            while (foundNewGroup)
            {
                foundNewGroup = false;
                
                foreach (var currentGroup in currentGroups)
                {
                    if (parentGroups.Contains(currentGroup.Name))
                    {
                        continue;
                    }
                    
                    foreach (var parentGroup in parentGroups.ToList())
                    {
                        if (ChildGroups(currentGroup).Contains(parentGroup))
                        {
                            parentGroups.Add(currentGroup.Name);
                            foundNewGroup = true;
                        }
                    }
                }
            }
            
            return parentGroups.ToArray();
        }

        public IGroup FindGroup(string name)
        {
            return this.FindGroupByName(name);
        }
        
        public void AddGroup(string groupName, string description)
        {
            var group = this.FindGroupByName(groupName);
            if (group != null)
            {
                return;
            }
            
            this.StoreItem(new Group
                           {
                               Name = groupName,
                               Description = description,
                               MemberGroups = new string[0],
                               MemberUsers = new string[0]
                           });
        }

        public void UpdateGroupByName(string groupName, string description)
        {
        
            var role = this.FindGroupByName(groupName);
            if (role == null)
            {
                return;
            }
            
            this.StoreItem(new Group
                           {
                               Name = role.Name,
                               Description = description,
                               MemberGroups = role.MemberGroups,
                               MemberUsers = role.MemberUsers
                           });
        }
        
        public void AssignUserToGroup(string groupName, string userName)
        {
            var group = this.FindGroupByName(groupName);
            if (group == null)
            {
                return;
            }
            
            var childUsers = ChildUsers(group);
            childUsers.Add(userName);
            group.MemberUsers = childUsers.ToArray();
            this.StoreItem(group);
        }
        
        public void AssignGroupToGroup(string containerGroupName, string groupName)
        {
            var group = this.FindGroupByName(containerGroupName);
            if (group == null)
            {
                return;
            }
            
            var childGroup = new HashSet<string>(group.MemberGroups ?? new string[0]);
            childGroup.Add(groupName);
            group.MemberGroups = childGroup.ToArray();
            this.StoreItem(group);
        }
        
        public void RemoveUserFromGroup(string groupName, string userName)
        {
            var group = this.FindGroupByName(groupName);
            if (group == null)
            {
                return;
            }
            
            var childUsers = new HashSet<string>(group.MemberUsers ?? new string[0]);
            childUsers.Remove(userName);
            group.MemberUsers = childUsers.ToArray();
            this.StoreItem(group);
        }

        public void RemoveGroupFromGroup(string containerGroupName, string groupName)
        {
            var group = this.FindGroupByName(containerGroupName);
            if (group == null)
            {
                return;
            }
            
            var childGroups = new HashSet<string>(group.MemberGroups ?? new string[0]);
            childGroups.Remove(groupName);
            group.MemberGroups = childGroups.ToArray();
            this.StoreItem(group);
        }

        public void RemoveGroup(string groupName)
        {
            var group = this.FindGroupByName(groupName);
            if (group == null)
            {
                return;
            }
            
            this.DeleteGroup(group);
        }
        
        protected Group FindGroupByName(string name)
        {
            try
            {
                return this.groups[name];
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void ResetGroups()
        {
            if (!File.Exists(this.groupFilePath))
            {
                File.WriteAllText(this.groupFilePath, "[]");
            }
            
            var file = File.ReadAllText(this.groupFilePath);
            var serializier = new Serialization.NewtonsoftSerializer();
            var currentUsers = serializier.Deserialize<List<Group>>(file);
            var collection = new MyKeyedCollection();
            
            foreach (var current in currentUsers)
            {
                collection.Add(current);
            }
            
            this.groups = collection;
        }
        
        private void DeleteGroup(Group group)
        {
            var file = File.ReadAllText(this.groupFilePath);
            var serializier = new Serialization.NewtonsoftSerializer();
            var currentGroups = serializier.Deserialize<List<Group>>(file);
            currentGroups.RemoveAll(x => string.Equals(x.Name, group.Name, StringComparison.OrdinalIgnoreCase));
            
            var output = serializier.Serialize<List<Group>>(currentGroups);
            File.WriteAllText(this.groupFilePath, output);
            this.ResetGroups();
        }
        
        private void StoreItem(Group group)
        {
            var file = File.ReadAllText(this.groupFilePath);
            var serializier = new Serialization.NewtonsoftSerializer();
            var currentGroups = serializier.Deserialize<List<Group>>(file);
            
            currentGroups.RemoveAll(x => string.Equals(x.Name, group.Name, StringComparison.OrdinalIgnoreCase));
            currentGroups.Add(group);
            
            var output = serializier.Serialize<List<Group>>(currentGroups);
            File.WriteAllText(this.groupFilePath, output);
            
            this.ResetGroups();
        }

        private static HashSet<string> ChildUsers(Group group)
        {
            return new HashSet<string>(group.MemberUsers ?? new string[0], StringComparer.OrdinalIgnoreCase);
        }

        private static HashSet<string> ChildGroups(Group group)
        {
            return new HashSet<string>(group.MemberGroups ?? new string[0], StringComparer.OrdinalIgnoreCase);
        }
        
        private class MyKeyedCollection : KeyedCollection<string, Group>
        {
            protected override string GetKeyForItem(Group item)
            {
                return item.Name;
            }
        }
    }
}
