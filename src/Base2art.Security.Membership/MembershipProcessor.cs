﻿namespace Base2art.Security.Membership
{
    using System;
    using System.Linq;
    using Repositories;
    
    public class MembershipProcessor : IMembershipProcessor
    {
        private readonly IMembershipGroupRepository repo;

        public MembershipProcessor(IMembershipGroupRepository repo)
        {
            this.repo = repo;
        }

        public Base2art.Collections.Specialized.IPagedCollection<IGroup> Groups(Base2art.Collections.Specialized.Page page)
        {
            return this.repo.Groups(page);
        }
        
        public IGroup FindGroupByName(string groupName)
        {
            return this.repo.FindGroup((groupName ?? string.Empty).Trim());
        }
        
        public string[] UserGroups(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
            {
                return new string[0];
            }
            
            return this.repo.UserGroups(userName);
        }

        public GroupCreationStatus CreateGroup(GroupData groupData)
        {
            if (string.IsNullOrWhiteSpace(groupData.Name))
            {
                return GroupCreationStatus.InvalidGroupName;
            }
            
            var group = this.repo.FindGroup(groupData.Name.Trim());
            if (group != null)
            {
                return GroupCreationStatus.DuplicateGroup;
            }
            
            this.repo.AddGroup(groupData.Name, groupData.Description);
            return GroupCreationStatus.Success;
        }

        public GroupCreationStatus UpdateGroup(GroupData groupData)
        {
            if (string.IsNullOrWhiteSpace(groupData.Name))
            {
                return GroupCreationStatus.InvalidGroupName;
            }
            
            var role = this.repo.FindGroup(groupData.Name.Trim());
            if (role == null)
            {
                return GroupCreationStatus.DuplicateGroup;
            }
            
            this.repo.UpdateGroupByName(groupData.Name, groupData.Description);
            return GroupCreationStatus.Success;
        }

        public GroupAssignmentStatus AssignUserToGroup(string groupName, string userName)
        {
            return this.ProcessAssigment(
                groupName,
                userName,
                GroupAssignmentStatus.DuplicateItem,
                (group) => !ChildUsers(group).Any(x => string.Equals(x, userName, StringComparison.OrdinalIgnoreCase)),
                (x, y) => this.repo.AssignUserToGroup(x, y));
        }

        public GroupAssignmentStatus AssignGroupToGroup(string containerGroupName, string groupName)
        {
            return this.ProcessAssigment(
                containerGroupName,
                groupName,
                GroupAssignmentStatus.DuplicateItem,
                (group) => !string.Equals(group.Name, groupName, StringComparison.OrdinalIgnoreCase) && !ChildGroups(group).Any(x => string.Equals(x, groupName, StringComparison.OrdinalIgnoreCase)),
                (x, y) => this.repo.AssignGroupToGroup(x, y));
        }

        public GroupAssignmentStatus RemoveUserFromGroup(string groupName, string userName)
        {
            return this.ProcessAssigment(
                groupName,
                userName,
                GroupAssignmentStatus.ItemNotFound,
                (group) => ChildUsers(group).Any(x => string.Equals(x, userName, StringComparison.OrdinalIgnoreCase)),
                (x, y) => this.repo.RemoveUserFromGroup(x, y));
        }
        
        public GroupAssignmentStatus RemoveGroupFromGroup(string containerGroupName, string groupName)
        {
            return this.ProcessAssigment(
                containerGroupName,
                groupName,
                GroupAssignmentStatus.ItemNotFound,
                (group) => ChildGroups(group).Any(x => string.Equals(x, groupName, StringComparison.OrdinalIgnoreCase)),
                (x, y) => this.repo.RemoveGroupFromGroup(x, y));
        }

        public GroupRemovalStatus RemoveGroup(string groupName)
        {
            if (string.IsNullOrWhiteSpace(groupName))
            {
                return GroupRemovalStatus.InvalidGroupName;
            }
            
            var group = this.repo.FindGroup(groupName.Trim());
            if (group == null)
            {
                return GroupRemovalStatus.GroupNotFound;
            }
            
            this.repo.RemoveGroup(groupName);
            return GroupRemovalStatus.Success;
        }
        

        private static string[] ChildUsers(IGroup group)
        {
            return group.MemberUsers ?? new string[0];
        }

        private static string[] ChildGroups(IGroup group)
        {
            return group.MemberGroups ?? new string[0];
        }

        private GroupAssignmentStatus ProcessAssigment(
            string parent,
            string child,
            GroupAssignmentStatus cantPerformActionStatus,
            Func<IGroup, bool> canPerformAction,
            Action<string, string> performAction)
        {
            if (string.IsNullOrWhiteSpace(parent))
            {
                return GroupAssignmentStatus.InvalidGroupName;
            }
            
            if (string.IsNullOrWhiteSpace(child))
            {
                return GroupAssignmentStatus.InvalidChildItem;
            }
            
            var group = this.repo.FindGroup(parent.Trim());
            if (group == null)
            {
                return GroupAssignmentStatus.GroupNotFound;
            }
            
            if (!canPerformAction(group))
            {
                return cantPerformActionStatus;
            }
            
            performAction(parent.Trim(), child.Trim());
            
            return GroupAssignmentStatus.Success;
        }
    }
}
