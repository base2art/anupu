﻿namespace Base2art.Security.Membership
{
	public enum GroupRemovalStatus
	{
		Success,
		GroupNotFound,
		InvalidGroupName,
	}
}
