﻿namespace Base2art.Security.Membership
{
	public enum GroupCreationStatus
	{
	    Success,
	    InvalidGroupName,
	    DuplicateGroup,
	}
}


