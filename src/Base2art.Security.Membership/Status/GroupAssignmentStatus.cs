﻿namespace Base2art.Security.Membership
{
	public enum GroupAssignmentStatus
	{
	    Success,
	    GroupNotFound,
	    InvalidGroupName,
	    DuplicateItem,
        ItemNotFound,
        InvalidChildItem
	}
}
