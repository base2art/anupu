﻿namespace Base2art.Security.Membership
{
	public interface IGroup
	{
		string Name { get; }
        
        string Description { get; }
		
		string[] MemberGroups { get; set; }
		
		string[] MemberUsers{ get; set; }
	}
}

