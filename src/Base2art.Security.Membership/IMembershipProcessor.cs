﻿namespace Base2art.Security.Membership
{
    using Base2art.Collections.Specialized;
    
    public interface IMembershipProcessor
    {
        string[] UserGroups(string userName);
        
        IPagedCollection<IGroup> Groups(Page page);

        IGroup FindGroupByName(string groupName);
        
        GroupCreationStatus CreateGroup(GroupData groupData);
        
        GroupCreationStatus UpdateGroup(GroupData groupData);
        
        GroupAssignmentStatus AssignUserToGroup(string groupName, string userName);
        
        GroupAssignmentStatus AssignGroupToGroup(string containerGroupName, string groupName);
        
        GroupAssignmentStatus RemoveUserFromGroup(string groupName, string userName);
        
        GroupAssignmentStatus RemoveGroupFromGroup(string containerGroupName, string groupName);
        
        GroupRemovalStatus RemoveGroup(string groupName);
    }
}