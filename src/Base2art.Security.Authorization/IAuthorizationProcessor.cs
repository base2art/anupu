﻿namespace Base2art.Security.Authorization
{
    using System;
    using Base2art.Collections.Specialized;
    
    public interface IAuthorizationProcessor
    {
        bool CanAccessUri(Uri uri, string userName, string[] userRoles);
        
        CreatePermissionStatus AddWhiteListPermission(UriMapFilterData uri);

        CreatePermissionStatus AddRoleBasedPermission(RoleBasedPermissionData permissionData);
        
        RemovePermissionStatus RemoveWhiteListPermission(UriMapFilterData uri);

        RemovePermissionStatus RemoveRoleBasedPermission(UriMapFilterData uri);
        
        IPagedCollection<IUriMapFilter> WhiteListPermissions(Page page);
        
        IPagedCollection<IUriMap> RoleBasedPermissions(Page page);
    }
}
