﻿namespace Base2art.Security.Authorization
{
    public interface IUriMap
    {
	    IUriMapFilter Uri { get; }

		string[] Users { get; }

		string[] Roles { get; }
		
		AccessLevel AccessLevel { get; }
    }
    
}
