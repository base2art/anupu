﻿namespace Base2art.Security.Authorization
{
	public class UriMapFilterData
	{
	    public string Host { get; set; }

		public string Path { get; set; }

		public bool CaseSensitive { get; set; }

		public int? Port { get; set; }
	}
}




