﻿namespace Base2art.Security.Authorization.Repositories.Models
{
	public class UriMap : IUriMap
	{
	    public UriMapFilter Uri { get; set; }

		public string[] Users { get; set; }

		public string[] Roles { get; set; }
		
		public AccessLevel AccessLevel { get; set; }

        IUriMapFilter IUriMap.Uri
        {
            get { return this.Uri; }
        }
	}
}
