﻿namespace Base2art.Security.Authorization.Repositories.Models
{
	public class UriMapFilter : IUriMapFilter
	{
		public string Host { get; set; }

		public string Path { get; set; }

		public bool CaseSensitive { get; set; }

		public int? Port { get; set; }
	}
}

