﻿namespace Base2art.Security.Authorization.Repositories.Models
{
    public class AuthorizationData
    {
        public UriMapFilter[] WhiteListUrls { get; set; }
        
        public UriMap[] RestrictedUrls { get; set; }
    }
}
