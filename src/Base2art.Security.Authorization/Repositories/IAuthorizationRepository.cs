﻿namespace Base2art.Security.Authorization.Repositories
{
    using System;
    using Base2art.Collections.Specialized;
    
    public interface IAuthorizationRepository
    {
        bool HasWhiteListEntries { get; }
        
        bool IsUriWhiteListed(Uri uri);
        
        IUriMap FindRoleBasedPermission(Uri uri);
        
        void AddRoleBasedPermission(
            string host,
            string path,
            bool caseSensitive,
            int? port,
            string[] users,
            string[] roles,
            AccessLevel accessLevel);

        void AddWhiteListPermission(
            string host,
            string path,
            bool caseSensitive,
            int? port);

        IPagedCollection<IUriMapFilter> WhiteListPermissions(Page page);

        IPagedCollection<IUriMap> RoleBasedPermissions(Page page);

        int RemoveRoleBasedPermission(
            string host,
            string path,
            bool caseSensitive,
            int? port);

        int RemoveWhiteListPermission(
            string host,
            string path,
            bool caseSensitive,
            int? port);
    }
}
