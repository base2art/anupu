﻿namespace Base2art.Security.Authorization.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Base2art.Collections.Specialized;
    using Models;

    public class JsonAuthorizationRepository : IAuthorizationRepository
    {
        private List<UriMap> restrictedUrls = new List<UriMap>();
        private List<UriMapFilter> whiteListUrls = new List<UriMapFilter>();

        private readonly string urlsFilePath;
        
        public JsonAuthorizationRepository(string urlsFilePath)
        {
            this.urlsFilePath = urlsFilePath;
            this.ResetUrls();
        }

        public bool HasWhiteListEntries
        {
            get { return this.whiteListUrls.Count > 0; }
        }
        
        private UriMap[] UriMaps
        {
            get { return this.restrictedUrls.ToArray(); }
        }

        public IUriMap FindRoleBasedPermission(Uri uri)
        {
            var item = this.FindMatch(this.restrictedUrls, x => x.Uri ?? new UriMapFilter(), uri);
            return item;
        }

        public void AddRoleBasedPermission(
            string host,
            string path,
            bool caseSensitive,
            int? port,
            string[] users,
            string[] roles,
            AccessLevel accessLevel)
        {
            var uriMap = new UriMap
            {
                AccessLevel = accessLevel,
                Users = users ?? new string[0],
                Roles = roles ?? new string[0],
                Uri = new UriMapFilter
                {
                    CaseSensitive = caseSensitive,
                    Host = host,
                    Port = port,
                    Path = path
                }
            };
            
            this.StoreItem(uriMap);
        }
        
        public bool IsUriWhiteListed(Uri uri)
        {
            var item = this.FindMatch(this.whiteListUrls, x => x, uri);
            return item != null;
        }

        public void AddWhiteListPermission(string host, string path, bool caseSensitive, int? port)
        {
            this.StoreItem(new UriMapFilter
                           {
                               CaseSensitive = caseSensitive,
                               Host = host,
                               Port = port,
                               Path = path
                           });
        }

        public IPagedCollection<IUriMapFilter> WhiteListPermissions(Page page)
        {
            var items = this.whiteListUrls;
            return new PagedCollection<IUriMapFilter>(items.Skip(page.Start()).Take(page.PageSize), page, items.Count);
        }
        
        public IPagedCollection<IUriMap> RoleBasedPermissions(Page page)
        {
            var items = this.restrictedUrls;
            return new PagedCollection<IUriMap>(items.Skip(page.Start()).Take(page.PageSize), page, items.Count);
        }

        public int RemoveRoleBasedPermission(string host, string path, bool caseSensitive, int? port)
        {
            UriMapFilter map = new UriMapFilter { Host = host, Path = path, CaseSensitive = caseSensitive, Port = port };
            return this.DeleteRoleBasedPermissions(map);
        }

        public int RemoveWhiteListPermission(string host, string path, bool caseSensitive, int? port)
        {
            UriMapFilter map = new UriMapFilter { Host = host, Path = path, CaseSensitive = caseSensitive, Port = port };
            return this.DeleteWhiteListPermissions(map);
        }
        
        private T FindMatch<T>(List<T> list, Func<T, UriMapFilter> mapper, Uri uri)
            where T : class
        {
            Func<UriMapFilter, Uri, bool> hostMatches = (x,y) =>
            {
                if (string.IsNullOrWhiteSpace(x.Host))
                {
                    return true;
                }
                
                return string.Equals(x.Host, y.Host, StringComparison.OrdinalIgnoreCase);
            };
            
            Func<UriMapFilter, Uri, bool> pathMatches = (x,y) =>
            {
                if (string.IsNullOrWhiteSpace(x.Path))
                {
                    return true;
                }
                
                var comp = x.CaseSensitive ? StringComparison.Ordinal :  StringComparison.OrdinalIgnoreCase;
                return string.Equals(x.Path, y.AbsolutePath, comp);
            };
            
            Func<UriMapFilter, Uri, bool> portMatches = (x,y) =>
            {
                if (!x.Port.HasValue)
                {
                    return true;
                }
                
                return y.Port == x.Port.Value;
            };
            
            foreach (var item in list.Where(x => x != null))
            {
                var mappedValue = mapper(item);
                
                if (hostMatches(mappedValue, uri) && pathMatches(mappedValue, uri) && portMatches(mappedValue, uri))
                {
                    return item;
                }
            }
            
            return null;
        }
        
        private void ResetUrls()
        {
            var serializier = new Serialization.NewtonsoftSerializer();
            
            if (!File.Exists(this.urlsFilePath))
            {
                var authData = new AuthorizationData();
                authData.RestrictedUrls = new UriMap[0];
                authData.WhiteListUrls = new UriMapFilter[0];
                File.WriteAllText(this.urlsFilePath, serializier.Serialize(authData));
            }
            
            var file = File.ReadAllText(this.urlsFilePath);
            var currentUsers = serializier.Deserialize<AuthorizationData>(file);
            
            var parsedRestrictedUrls = new List<UriMap>(currentUsers.RestrictedUrls ?? new UriMap[0]);
            this.restrictedUrls = parsedRestrictedUrls;
            
            var parsedWhiteListUrls = new List<UriMapFilter>(currentUsers.WhiteListUrls ?? new UriMapFilter[0]);
            this.whiteListUrls = parsedWhiteListUrls;
        }
        
        
        private int DeleteRoleBasedPermissions(UriMapFilter map)
        {
            var file = File.ReadAllText(this.urlsFilePath);
            var serializier = new Serialization.NewtonsoftSerializer();
            var currentUsers = serializier.Deserialize<AuthorizationData>(file);
            
            var items = currentUsers.RestrictedUrls
                .Where(x => string.Equals(x.Uri.Host ?? string.Empty, map.Host ?? string.Empty, StringComparison.OrdinalIgnoreCase))
                .Where(x => string.Equals(x.Uri.Path ?? string.Empty, map.Path ?? string.Empty, map.CaseSensitive ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase))
                .Where(x => x.Uri.CaseSensitive ==  map.CaseSensitive)
                .Where(x => x.Uri.Port ==  map.Port);
            
            currentUsers.RestrictedUrls = currentUsers.RestrictedUrls.Except(items).ToArray();
            
            var output = serializier.Serialize<AuthorizationData>(currentUsers);
            File.WriteAllText(this.urlsFilePath, output);
            this.ResetUrls();
            return items.Count();
        }
        
        private int DeleteWhiteListPermissions(UriMapFilter map)
        {
            var file = File.ReadAllText(this.urlsFilePath);
            var serializier = new Serialization.NewtonsoftSerializer();
            var currentUsers = serializier.Deserialize<AuthorizationData>(file);
            
            var items = currentUsers.WhiteListUrls
                .Where(x => string.Equals(x.Host ?? string.Empty, map.Host ?? string.Empty, StringComparison.OrdinalIgnoreCase))
                .Where(x => string.Equals(x.Path ?? string.Empty, map.Path ?? string.Empty, map.CaseSensitive ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase))
                .Where(x => x.CaseSensitive ==  map.CaseSensitive)
                .Where(x => x.Port ==  map.Port);
            
            currentUsers.WhiteListUrls = currentUsers.WhiteListUrls.Except(items).ToArray();
            
            var output = serializier.Serialize<AuthorizationData>(currentUsers);
            File.WriteAllText(this.urlsFilePath, output);
            this.ResetUrls();
            return items.Count();
        }
        
        private void StoreItem(UriMap map)
        {
            var file = File.ReadAllText(this.urlsFilePath);
            var serializier = new Serialization.NewtonsoftSerializer();
            var currentUsers = serializier.Deserialize<AuthorizationData>(file);
            
            currentUsers.RestrictedUrls = currentUsers.RestrictedUrls.Union(new[]{ map }).ToArray();
            
            var output = serializier.Serialize<AuthorizationData>(currentUsers);
            File.WriteAllText(this.urlsFilePath, output);
            
            this.ResetUrls();
        }
        
        private void StoreItem(UriMapFilter map)
        {
            var file = File.ReadAllText(this.urlsFilePath);
            var serializier = new Serialization.NewtonsoftSerializer();
            var currentUsers = serializier.Deserialize<AuthorizationData>(file);
            
            currentUsers.WhiteListUrls = currentUsers.WhiteListUrls.Union(new[]{ map }).ToArray();
            
            var output = serializier.Serialize<AuthorizationData>(currentUsers);
            File.WriteAllText(this.urlsFilePath, output);
            
            this.ResetUrls();
        }
    }
}
