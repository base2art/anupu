﻿namespace Base2art.Security.Authorization
{
    using System;
    using System.Collections.Generic;
    using Repositories;

    public class AuthorizationProcessor : IAuthorizationProcessor
    {
        private readonly IAuthorizationRepository repository;

        public AuthorizationProcessor(IAuthorizationRepository repository)
        {
            this.repository = repository;
        }
        
        public bool CanAccessUri(Uri uri, string userName, string[] userRoles)
        {
            if (string.IsNullOrWhiteSpace(userName))
            {
                if (!this.repository.HasWhiteListEntries)
                {
                    return false;
                }
                
                var urlIsWhiteListed = this.repository.IsUriWhiteListed(uri);
                
                if (urlIsWhiteListed)
                {
                    return true;
                }
            }
            
            var uriMap = this.repository.FindRoleBasedPermission(uri);
            
            if (uriMap == null)
            {
                return false;
            }
            
            var userNames = new HashSet<string>(uriMap.Users ?? new string[0]);
            
            if (userNames.Contains(userName))
            {
                return uriMap.AccessLevel == AccessLevel.Allow;
            }
            
            var allowedRoles = new HashSet<string>(uriMap.Roles ?? new string[0]);
            userRoles = userRoles ?? new string[0];
            
            foreach (var role in userRoles)
            {
                if (allowedRoles.Contains(role))
                {
                    return uriMap.AccessLevel == AccessLevel.Allow;
                }
            }
            
            if (userNames.Count + allowedRoles.Count == 0)
            {
                return uriMap.AccessLevel == AccessLevel.Allow;
            }
            
            return false;
        }
        
        public CreatePermissionStatus AddRoleBasedPermission(RoleBasedPermissionData permissionData)
        {
            this.repository.AddRoleBasedPermission(
                permissionData.Uri.Host,
                permissionData.Uri.Path,
                permissionData.Uri.CaseSensitive,
                permissionData.Uri.Port,
                permissionData.Users,
                permissionData.Roles,
                permissionData.AccessLevel);
            
            return CreatePermissionStatus.Success;
        }

        public CreatePermissionStatus AddWhiteListPermission(UriMapFilterData uri)
        {
            this.repository.AddWhiteListPermission(
                uri.Host,
                uri.Path,
                uri.CaseSensitive,
                uri.Port);
            
            return CreatePermissionStatus.Success;
        }

        public Base2art.Collections.Specialized.IPagedCollection<IUriMapFilter> WhiteListPermissions(Base2art.Collections.Specialized.Page page)
        {
            return this.repository.WhiteListPermissions(page);
        }

        public Base2art.Collections.Specialized.IPagedCollection<IUriMap> RoleBasedPermissions(Base2art.Collections.Specialized.Page page)
        {
            return this.repository.RoleBasedPermissions(page);
        }
        
        public RemovePermissionStatus RemoveWhiteListPermission(UriMapFilterData uri)
        {
            var itemsRemoved = this.repository.RemoveWhiteListPermission(uri.Host, uri.Path, uri.CaseSensitive, uri.Port);
            if (itemsRemoved == 0)
            {
                return RemovePermissionStatus.NoMatches;
            }
            
            if (itemsRemoved == 1)
            {
                return RemovePermissionStatus.Success_OneMatch;
            }
            
            return RemovePermissionStatus.Success_ManyMatches;
        }
        
        public RemovePermissionStatus RemoveRoleBasedPermission(UriMapFilterData uri)
        {
            var itemsRemoved = this.repository.RemoveRoleBasedPermission(uri.Host, uri.Path, uri.CaseSensitive, uri.Port);
            if (itemsRemoved == 0)
            {
                return RemovePermissionStatus.NoMatches;
            }
            
            if (itemsRemoved == 1)
            {
                return RemovePermissionStatus.Success_OneMatch;
            }
            
            return RemovePermissionStatus.Success_ManyMatches;
        }
    }
}

