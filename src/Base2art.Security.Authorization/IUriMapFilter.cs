﻿namespace Base2art.Security.Authorization
{
	public interface IUriMapFilter
	{
		string Host { get; set; }

		string Path { get; set; }
		
		bool CaseSensitive { get; set; }
		
		int? Port { get; set; }
	}
}


