﻿namespace Base2art.Security.Authorization
{
	public class RoleBasedPermissionData
	{
	    public UriMapFilterData Uri { get; set; }

		public string[] Users { get; set; }

		public string[] Roles { get; set; }
		
		public AccessLevel AccessLevel { get; set; }
	}
}


