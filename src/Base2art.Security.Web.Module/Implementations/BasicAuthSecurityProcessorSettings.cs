﻿namespace Base2art.Security.Web.Module.Implementations
{
	public class BasicAuthSecurityProcessorSettings : IBasicAuthSecurityProcessorSettings
	{
		public string Realm { get; set; }

		public string DefaultRole { get; set; }
	}
}

