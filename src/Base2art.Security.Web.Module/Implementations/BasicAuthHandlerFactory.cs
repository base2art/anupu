﻿namespace Base2art.Security.Web.Module.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Base2art.Security.AccessControl;
    using Base2art.Security.Authentication;
    using Base2art.Security.Authorization;
    using Base2art.Security.Characterization;
    using Base2art.Security.Membership;
    
    public class BasicAuthSecurityProcessorFactory : ISecurityProcessorFactory
    {
        private readonly IBasicAuthSecurityProcessorSettings settings;
        
        public BasicAuthSecurityProcessorFactory(IBasicAuthSecurityProcessorSettings settings)
        {
            this.settings = settings;
        }

        public ISecurityHeaderRoleBasedProcessor CreateHeaderRoleBasedProcessor(
            IAuthenticationProcesssor authenticationProcessor,
            IMembershipProcessor membershipProcessor,
            ICharacterizationProcessor characterizationProcessor,
            IAuthorizationProcessor authorizationProcessor)
        {
            return new BasicAuthHeaderHandler(
                this.settings,
                authenticationProcessor,
                authorizationProcessor,
                characterizationProcessor,
                membershipProcessor);
        }
        
        public ISecurityCookieRoleBasedProcessor CreateCookieRoleBasedProcessor(
            IAuthenticationProcesssor authenticationProcessor,
            IMembershipProcessor membershipProcessor,
            ICharacterizationProcessor characterizationProcessor,
            IAuthorizationProcessor authorizationProcessor)
        {
            throw new NotImplementedException();
        }
        
        public ISecurityNoAuthRoleBasedProcessor CreateNoAuthTokenRequestRoleBasedProcessor(
            IAuthenticationProcesssor authenticationProcessor,
            IMembershipProcessor membershipProcessor,
            ICharacterizationProcessor characterizationProcessor,
            IAuthorizationProcessor authorizationProcessor)
        {
            return new BasicAuthHeaderHandler(
                this.settings,
                authenticationProcessor,
                authorizationProcessor,
                characterizationProcessor,
                membershipProcessor);
        }
        
        public ISecurityMalformedRequestRoleBasedProcessor CreateMalformedRequestRoleBasedProcessor(
            IAuthenticationProcesssor authenticationProcessor,
            IMembershipProcessor membershipProcessor,
            ICharacterizationProcessor characterizationProcessor,
            IAuthorizationProcessor authorizationProcessor)
        {
            return new BasicAuthHeaderHandler(
                this.settings,
                authenticationProcessor,
                authorizationProcessor,
                characterizationProcessor,
                membershipProcessor);
        }
        
        public ISecurityHeaderProcessor CreateHeaderProcessor(
            IAuthenticationProcesssor authenticationProcessor,
            IMembershipProcessor membershipProcessor,
            ICharacterizationProcessor characterizationProcessor,
            IAuthorizationProcessor authorizationProcessor)
        {
            return new BasicAuthHeaderHandler(
                this.settings,
                authenticationProcessor,
                authorizationProcessor,
                characterizationProcessor,
                membershipProcessor);
        }
        
        public ISecurityCookieProcessor CreateCookieProcessor(
            IAuthenticationProcesssor authenticationProcessor,
            IMembershipProcessor membershipProcessor,
            ICharacterizationProcessor characterizationProcessor,
            IAuthorizationProcessor authorizationProcessor)
        {
            throw new NotImplementedException();
        }
        
        public ISecurityNoAuthProcessor CreateNoAuthTokenRequestProcessor(
            IAuthenticationProcesssor authenticationProcessor,
            IMembershipProcessor membershipProcessor,
            ICharacterizationProcessor characterizationProcessor,
            IAuthorizationProcessor authorizationProcessor)
        {
            return new BasicAuthHeaderHandler(
                this.settings,
                authenticationProcessor,
                authorizationProcessor,
                characterizationProcessor,
                membershipProcessor);
        }
        
        public ISecurityMalformedRequestProcessor CreateMalformedRequestProcessor(
            IAuthenticationProcesssor authenticationProcessor,
            IMembershipProcessor membershipProcessor,
            ICharacterizationProcessor characterizationProcessor,
            IAuthorizationProcessor authorizationProcessor)
        {
            return new BasicAuthHeaderHandler(
                this.settings,
                authenticationProcessor,
                authorizationProcessor,
                characterizationProcessor,
                membershipProcessor);
        }
        
        private class BasicAuthHeaderHandler
            : ISecurityHeaderProcessor, ISecurityMalformedRequestProcessor, ISecurityNoAuthProcessor,
        ISecurityHeaderRoleBasedProcessor, ISecurityMalformedRequestRoleBasedProcessor, ISecurityNoAuthRoleBasedProcessor
        {
            private readonly IAuthenticationProcesssor authenticationProcessor;

            private readonly IAuthorizationProcessor authorizationProcessor;
            
            private readonly IMembershipProcessor membershipProcessor;

            private readonly IBasicAuthSecurityProcessorSettings settings;

            private readonly ICharacterizationProcessor characterizationProcessor;
            
            public BasicAuthHeaderHandler(
                IBasicAuthSecurityProcessorSettings settings,
                IAuthenticationProcesssor authenticationProcessor,
                IAuthorizationProcessor authorizationProcessor,
                ICharacterizationProcessor characterizationProcessor,
                IMembershipProcessor membershipProcessor)
            {
                this.characterizationProcessor = characterizationProcessor;
                this.membershipProcessor = membershipProcessor;
                this.settings = settings;
                this.authorizationProcessor = authorizationProcessor;
                this.authenticationProcessor = authenticationProcessor;
            }
            
            
            public ISecurityProcessorResult ProcessMalformedRequestRoleBased(string requiredRole)
            {
                return this.Result(SecurityStatus.InvalidRequest);
            }

            public ISecurityProcessorResult ProcessNoAuthRoleBased(string requiredRole)
            {
                return this.Result(SecurityStatus.UnknownUser);
            }
            
            public ISecurityProcessorResult ProcessCookieRoleBased(string cookieValue, string requiredRole)
            {
                return this.Process(cookieValue, requiredRole);
            }
            
            public ISecurityProcessorResult ProcessHeaderRoleBased(string headerValue, string requiredRole)
            {
                return this.Process(headerValue, requiredRole);
            }
            
            public ISecurityProcessorResult ProcessMalformedRequest(Uri uri)
            {
                return this.Result(SecurityStatus.InvalidRequest);
            }

            public ISecurityProcessorResult ProcessNoAuth(Uri uri)
            {
                if (!this.authenticationProcessor.HasUsers())
                {
                    return new SecurityProcessorResult(SecurityStatus.Success, string.Empty, new string[0]);
                }
                
                var result = this.authorizationProcessor.CanAccessUri(uri, string.Empty, new string[0]);
                if (result)
                {
                    return new SecurityProcessorResult(SecurityStatus.Success, string.Empty, new string[0]);
                }
                
                return this.Result(SecurityStatus.UnknownUser);
            }
            
            public ISecurityProcessorResult ProcessCookie(string cookieValue, Uri uri)
            {
                return this.Process(cookieValue, uri);
            }
            
            public ISecurityProcessorResult ProcessHeader(string headerValue, Uri uri)
            {
                return this.Process(headerValue, uri);
            }
            
            private ISecurityProcessorResult Process(string headerValue, string requiredRole)
            {
                return this.ProcessInternal(
                    headerValue,
                    (userName, roles) => {
                        var result = roles.Contains(requiredRole)
                            ? SecurityStatus.Success
                            : SecurityStatus.NoAccess;
                        
                        return this.SetupReturn(new SecurityProcessorResult(result, userName, roles.ToArray()));
                    });
            }
            
            private ISecurityProcessorResult Process(string headerValue, Uri uri)
            {
                return this.ProcessInternal(
                    headerValue,
                    (userName, roles) => {
                        var result = this.authorizationProcessor.CanAccessUri(uri, userName, roles.ToArray())
                            ? SecurityStatus.Success
                            : SecurityStatus.NoAccess;
                        
                        return this.SetupReturn(new SecurityProcessorResult(result, userName, roles.ToArray()));
                    });
            }

            private ISecurityProcessorResult ProcessInternal(string headerValue, Func<string, HashSet<string>, SecurityProcessorResult> evalute)
            {
                string authToken = headerValue;
                var authInfo = authToken.TrimEnd();
                var authInfoParts = authInfo.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (authInfoParts.Length != 2)
                {
                    return this.Result(SecurityStatus.InvalidRequest);
                }
                
                if (!string.Equals("Basic", authInfoParts[0].Trim(), StringComparison.InvariantCulture))
                {
                    return this.Result(SecurityStatus.InvalidRequest);
                }
                
                authInfo = authInfo.Substring(authInfo.LastIndexOf(' '));
                
                try
                {
                    var usernameAndPassword = this.Base64Decode(authInfo);
                    var parts = usernameAndPassword.Split(':');
                    if (parts.Length != 2)
                    {
                        return this.Result(SecurityStatus.InvalidRequest);
                    }
                    
                    var userData = new UserData
                    {
                        Name = parts[0].Trim(),
                        Password = parts[1].Trim()
                    };
                    
                    var signInResult = this.authenticationProcessor.SignIn(userData);
                    
                    if (signInResult != SignInStatus.Success)
                    {
                        return this.Result(signInResult.MapToSecurityStatus());
                    }
                    else
                    {
                        var userName = userData.Name;
                        var roles = RoleProvider.GetRoles(this.membershipProcessor, this.characterizationProcessor, userName);
                        
                        return evalute(userName, new HashSet<string>(roles ?? new string[0], StringComparer.OrdinalIgnoreCase));
                    }
                }
                catch (Exception e)
                {
                    throw new InvalidOperationException(authInfo, e);
                }
            }
            
            /// <summary>
            /// This is a utility methor that converts a string into
            ///		UT8 Encoded Base-64.
            /// </summary>
            /// <param name="data">The data to encode.</param>
            /// <returns>The encoded data.</returns>
            public string Base64Decode(string data)
            {
                byte[] todecode_byte = Convert.FromBase64String(data);
                return Encoding.UTF8.GetString(todecode_byte);
            }
            
            private ISecurityProcessorResult Result(SecurityStatus status)
            {
                var result = new SecurityProcessorResult(status);
                
                return this.SetupReturn(result);
            }

            private SecurityProcessorResult SetupReturn(SecurityProcessorResult result)
            {
                result.HeaderKey = "WWW-Authenticate";
                result.HeaderValue = string.Format("Basic realm={0}{1}{0}", '"', this.settings.Realm);
                return result;
            }
        }
    }
}

