﻿

namespace Base2art.Security.Web.Module
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Web;
    using Base2art.Cryptography;
    using Base2art.Cryptography.Algorithms;
    using Base2art.Security.AccessControl;
    using Base2art.Security.Authentication;
    using Base2art.Security.Authentication.Repositories;
    using Base2art.Security.Authorization;
    using Base2art.Security.Authorization.Repositories;
    using Base2art.Security.Characterization;
    using Base2art.Security.Characterization.Repositories;
    using Base2art.Security.Membership;
    using Base2art.Security.Membership.Repositories;
    using Base2art.Security.Web.Module.Implementations;
    
    public class HttpModule : IHttpModule
    {
        private static ISecurityService _processor;
        
        /// <summary>
        /// The Processor Instance
        /// </summary>
        public static ISecurityService Processor
        {
            get { return _processor; }
        }
        
        void System.Web.IHttpModule.Init(System.Web.HttpApplication application)
        {
            if (_processor == null)
            {
                var directory = application.Context.Server.MapPath("~/APP_DATA/base2art/");
                Directory.CreateDirectory(directory);
                var users_json = Path.Combine(directory, "auth_users.json");
                var groups_json = Path.Combine(directory, "auth_groups.json");
                var whitelist_json = Path.Combine(directory, "auth_whitelist.json");
                var characterization_json = Path.Combine(directory, "auth_characterization.json");
                
                // settings
                var securityProcessorSettings = new SecurityServiceSettings();
                var authenticationSettings = new AuthenticationProcessorSettings(TimeSpan.FromMinutes(30), CryptoAlgorithmSchema.Hashed_Sha512);
                var cryptorSettings = new CryptorSettings("My Company Secret");
                
                // services
                var emailService = new NullAuthenticationEmailService();
                var cryptor = new Cryptor(cryptorSettings);
                
                // repos
                var authenticationRepo = new JsonAuthenticationUserRepository(users_json);
                var membershipRepo = new JsonMembershipGroupRepository(groups_json);
                var authorizationRepo = new JsonAuthorizationRepository(whitelist_json);
                var characterizationRepo = new JsonCharacterizationRepository(characterization_json);
                
                
                // processors
                var authenticationProcessor = new AuthenticationProcessor(authenticationSettings, cryptor, emailService, authenticationRepo);
                var membershipProcessor = new MembershipProcessor(membershipRepo);
                var authorizationProcessor = new AuthorizationProcessor(authorizationRepo);
                var characterizationProcessor = new CharacterizationProcessor(characterizationRepo);
                
                var basicAuthSettings = new BasicAuthSecurityProcessorSettings { Realm = "Sign In" };
                
                _processor = new SecurityService(
                    securityProcessorSettings,
                    authenticationProcessor,
                    membershipProcessor,
                    characterizationProcessor,
                    authorizationProcessor,
                    new BasicAuthSecurityProcessorFactory(basicAuthSettings));
            }
            
            IDictionary<SecurityStatus, HttpStatusCode> map = new Dictionary<SecurityStatus, HttpStatusCode>
            {
                { SecurityStatus.BadPassword, HttpStatusCode.Unauthorized },
                { SecurityStatus.InvalidRequest, HttpStatusCode.Unauthorized },
                { SecurityStatus.LockedOut, HttpStatusCode.Unauthorized },
                { SecurityStatus.NoAccess, HttpStatusCode.Forbidden },
                { SecurityStatus.UnknownError, HttpStatusCode.Unauthorized },
                { SecurityStatus.UnknownUser, HttpStatusCode.Unauthorized },
            };
            
            application.BeginRequest += delegate(object sender, EventArgs e)
            {
                var app = sender as HttpApplication;
                
                var requestData = new RequestDataAccessor(app.Context);
                var responseData = new ResponseDataAccessor(app.Context);
                
                app.Context.Services().Add(_processor);
                
                var result = _processor.CanAccess(requestData, responseData);
                if (map.ContainsKey(result))
                {
                    app.Context.Response.StatusCode = (int)map[result];
                    app.Context.Response.ContentType = "text/plain";
                    app.Context.Response.Write(string.Format("[{0}].[{1}] Please Authenticate...", result.ToString("G"), map[result].ToString("G")));
                    app.Context.Response.End();
                    
                }
            };
        }
        
        void System.Web.IHttpModule.Dispose()
        {
        }
    }
}