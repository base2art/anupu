﻿namespace Base2art.Security.Web.Module
{
    using System;
    using System.Web;
    using Base2art.Security.AccessControl;
    
    public class RequestDataAccessor : IRequestDataAccessor
    {
        private readonly HttpContext context;

        public RequestDataAccessor(HttpContext context)
        {
            this.context = context;
        }

        public string Header(string authorizationHeader)
        {
            return this.context.Request.Headers[authorizationHeader];
        }

        public string Cookie(string cookieName)
        {
            var cookie = this.context.Request.Cookies[cookieName];
            if (cookie == null)
            {
                return null;
            }
            
            return cookie.Value;
        }

        public Uri Uri()
        {
            return this.context.Request.Url;
        }
    }
}

