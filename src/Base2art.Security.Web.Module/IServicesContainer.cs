﻿namespace Base2art.Security.Web.Module
{
	public interface IServicesContainer
	{
	    T Find<T>() where T : class;
	    
		void Add<T>(T service) where T : class;

		void Replace<T>(T service) where T : class;
	}
}


