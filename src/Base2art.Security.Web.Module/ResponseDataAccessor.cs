﻿namespace Base2art.Security.Web.Module
{
    using System;
    using System.Web;
    using Base2art.Security.AccessControl;
    
    public class ResponseDataAccessor : IResponseDataAccessor
    {
        private readonly HttpContext context;

        public ResponseDataAccessor(HttpContext context)
        {
            this.context = context;
        }

        public void Header(string authorizationHeader, string value)
        {
            this.context.Response.AddHeader(authorizationHeader, value);
        }

        public void Cookie(string cookieName, string value)
        {
            this.context.Response.Cookies.Add(new HttpCookie(cookieName, value));
        }

        public void Uri(string url)
        {
            this.context.Response.RedirectLocation = url;
        }
    }
}

