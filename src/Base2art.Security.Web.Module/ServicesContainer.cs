﻿namespace Base2art.Security.Web.Module
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Web;
    
    public static class ServicesContainer
    {
        private static readonly Guid keyValue = Guid.NewGuid();
        
        public static IServicesContainer Services(this HttpContext context)
        {
            if (context == null)
            {
                return null;
            }
            
            var key = keyValue.ToString("N");
            if (!context.Items.Contains(key))
            {
                var value1 = new ServicesContainerImpl();
                context.Items[key] = value1;
                return value1;
            }
            
            var value = context.Items[key] as ServicesContainerImpl;
            if (value == null)
            {
                value = new ServicesContainerImpl();
                context.Items[key] = value;
            }
            
            return value;
        }
        
        private class ServicesContainerImpl : IServicesContainer
        {
            private readonly IDictionary<Type, object> collection = new CustomKeyedCollection();
            
            public T Find<T>()
                where T : class
            {
                if (this.collection.ContainsKey(typeof(T)))
                {
                    return this.collection[typeof(T)] as T;
                }
                
                return null;
            }

            public void Replace<T>(T service)
                where T : class
            {
                if (service == null)
                {
                    return;
                }
                
                if (this.collection.ContainsKey(typeof(T)))
                {
                    this.collection.Remove(typeof(T));
                    this.collection.Add(typeof(T), service);
                }
            }
            
            public void Add<T>(T service)
                where T : class
            {
                if (service == null)
                {
                    return;
                }
                
                if (this.collection.ContainsKey(typeof(T)))
                {
                    throw new ArgumentException("Duplicate service registered");
                }
                
                this.collection.Add(typeof(T), service);
            }
            
            private class CustomKeyedCollection : Dictionary<Type, object>
            {
            }
        }
    }
}
