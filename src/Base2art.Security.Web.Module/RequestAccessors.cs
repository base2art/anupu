﻿namespace Base2art.Security.Web.Module
{
	using System;
	using System.Web;
	using Base2art.Security.AccessControl;

	public static class RequestAccessors
	{
		public static IRequestDataAccessor RequestData(this HttpContext context)
		{
			return new RequestDataAccessor(context);
		}

		public static IResponseDataAccessor ResponseData(this HttpContext context)
		{
			return new ResponseDataAccessor(context);
		}
	}
}



