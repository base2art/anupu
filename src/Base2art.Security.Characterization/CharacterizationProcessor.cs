﻿namespace Base2art.Security.Characterization
{
    using System;
    using System.Linq;
    using Repositories;
    
    public class CharacterizationProcessor : ICharacterizationProcessor
    {
        private readonly ICharacterizationRepository repo;

        public CharacterizationProcessor(ICharacterizationRepository repo)
        {
            this.repo = repo;
        }

        public Base2art.Collections.Specialized.IPagedCollection<IRole> Roles(Base2art.Collections.Specialized.Page page)
        {
            return this.repo.Roles(page);
        }

        public IRole FindRoleByName(string roleName)
        {
            return this.repo.FindRole(roleName);
        }
        
        public string[] UserRoles(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
            {
                return new string[0];
            }
            
            return this.repo.UserRoles(userName);
        }

        public string[] GroupRoles(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
            {
                return new string[0];
            }
            
            return this.repo.GroupRoles(userName);
        }

        public RoleCreationStatus CreateRole(RoleData roleData)
        {
            if (string.IsNullOrWhiteSpace(roleData.Name))
            {
                return RoleCreationStatus.InvalidGroupName;
            }
            
            var role = this.repo.FindRole(roleData.Name.Trim());
            if (role != null)
            {
                return RoleCreationStatus.DuplicateGroup;
            }
            
            this.repo.AddRole(roleData.Name, roleData.Description);
            return RoleCreationStatus.Success;
        }

        public RoleCreationStatus UpdateRole(RoleData roleData)
        {
            if (string.IsNullOrWhiteSpace(roleData.Name))
            {
                return RoleCreationStatus.InvalidGroupName;
            }
            
            var role = this.repo.FindRole(roleData.Name.Trim());
            if (role == null)
            {
                return RoleCreationStatus.DuplicateGroup;
            }
            
            this.repo.UpdateRoleByName(roleData.Name, roleData.Description);
            return RoleCreationStatus.Success;
        }
        
        public RoleAssignmentStatus AssignUserToRole(string roleName, string userName)
        {
            return this.ProcessAssigment(
                roleName,
                userName,
                RoleAssignmentStatus.DuplicateItem,
                (role) => !MemberUsers(role).Any(x => string.Equals(x, userName, StringComparison.OrdinalIgnoreCase)),
                (x, y) => this.repo.AssignUserToRole(x, y));
        }

        public RoleAssignmentStatus AssignGroupToRole(string roleName, string groupName)
        {
            return this.ProcessAssigment(
                roleName,
                groupName,
                RoleAssignmentStatus.DuplicateItem,
                (role) => !MemberGroups(role).Any(x => string.Equals(x, groupName, StringComparison.OrdinalIgnoreCase)),
                (x, y) => this.repo.AssignGroupToRole(x, y));
        }

        public RoleAssignmentStatus RemoveUserFromRole(string roleName, string userName)
        {
            return this.ProcessAssigment(
                roleName,
                userName,
                RoleAssignmentStatus.InvalidChildItem,
                (role) => MemberUsers(role).Any(x => string.Equals(x, userName, StringComparison.OrdinalIgnoreCase)),
                (x, y) => this.repo.RemoveUserFromRole(x, y));
        }

        public RoleAssignmentStatus RemoveGroupFromRole(string roleName, string groupName)
        {
            return this.ProcessAssigment(
                roleName,
                groupName,
                RoleAssignmentStatus.InvalidChildItem,
                (role) => MemberGroups(role).Any(x => string.Equals(x, groupName, StringComparison.OrdinalIgnoreCase)),
                (x, y) => this.repo.RemoveGroupFromRole(x, y));
        }

        public RoleRemovalStatus RemoveRole(string roleName)
        {
            if (string.IsNullOrWhiteSpace(roleName))
            {
                return RoleRemovalStatus.InvalidRoleName;
            }
            
            var role = this.repo.FindRole(roleName.Trim());
            if (role == null)
            {
                return RoleRemovalStatus.RoleNotFound;
            }
            
            this.repo.RemoveRole(roleName);
            return RoleRemovalStatus.Success;
        }
        

        private static string[] MemberUsers(IRole role)
        {
            return role.Users ?? new string[0];
        }

        private static string[] MemberGroups(IRole role)
        {
            return role.Groups ?? new string[0];
        }

        private RoleAssignmentStatus ProcessAssigment(
            string parent,
            string child,
            RoleAssignmentStatus cantPerformActionStatus,
            Func<IRole, bool> canPerformAction,
            Action<string, string> performAction)
        {
            if (string.IsNullOrWhiteSpace(parent))
            {
                return RoleAssignmentStatus.InvalidGroupName;
            }
            
            if (string.IsNullOrWhiteSpace(child))
            {
                return RoleAssignmentStatus.InvalidChildItem;
            }
            
            var role = this.repo.FindRole(parent.Trim());
            if (role == null)
            {
                return RoleAssignmentStatus.GroupNotFound;
            }
            
            if (!canPerformAction(role))
            {
                return cantPerformActionStatus;
            }
            
            performAction(parent.Trim(), child.Trim());
            
            return RoleAssignmentStatus.Success;
        }
    }
}
