﻿namespace Base2art.Security.Characterization
{
    using Base2art.Collections.Specialized;
    
    public interface ICharacterizationProcessor
    {
        string[] UserRoles(string userName);
        
        string[] GroupRoles(string userName);

        IPagedCollection<IRole> Roles(Page page);

        IRole FindRoleByName(string roleName);

        RoleCreationStatus UpdateRole(RoleData roleData);
        
        RoleCreationStatus CreateRole(RoleData roleData);
        
        RoleAssignmentStatus AssignUserToRole(string roleName, string userName);
        
        RoleAssignmentStatus AssignGroupToRole(string roleName, string groupName);
        
        RoleAssignmentStatus RemoveUserFromRole(string roleName, string userName);
        
        RoleAssignmentStatus RemoveGroupFromRole(string roleName, string groupName);
        
        RoleRemovalStatus RemoveRole(string roleName);
    }
}
