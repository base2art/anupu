﻿namespace Base2art.Security.Characterization
{
    public class RoleData
    {
        public string Name { get; set; }
        
        public string Description { get; set; }
    }
}
