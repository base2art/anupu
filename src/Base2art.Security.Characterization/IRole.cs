﻿namespace Base2art.Security.Characterization
{
    public interface IRole
    {
        string Name { get; }
        
        string Description { get; }
        
        string[] Users { get; }
        
        string[] Groups { get; }
    }
}