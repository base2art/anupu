﻿namespace Base2art.Security.Characterization
{
	public enum RoleCreationStatus
	{
	    Success,
	    InvalidGroupName,
	    DuplicateGroup,
	}
}


