﻿namespace Base2art.Security.Characterization
{
	public enum RoleAssignmentStatus
	{
	    Success,
	    GroupNotFound,
	    InvalidGroupName,
	    DuplicateItem,
        ItemNotFound,
        InvalidChildItem
	}
}
