﻿namespace Base2art.Security.Characterization
{
	public enum RoleRemovalStatus
	{
		Success,
		RoleNotFound,
		InvalidRoleName,
	}
}
