﻿
namespace Base2art.Security.Characterization.Repositories.Models
{
    public class Role : IRole
    {
        public string Name { get; set; }

        public string Description { get; set; }
        
        public string[] Users { get; set; }
            
        public string[] Groups { get; set; }
    }
}
