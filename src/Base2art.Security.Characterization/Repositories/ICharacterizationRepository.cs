﻿namespace Base2art.Security.Characterization.Repositories
{
    using Base2art.Collections.Specialized;
    
    public interface ICharacterizationRepository
    {
        IPagedCollection<IRole> Roles(Page page);

        string[] UserRoles(string userName);

        string[] GroupRoles(string groupName);

        void AddRole(string name, string description);

        void UpdateRoleByName(string name, string description);
        
        IRole FindRole(string name);

        void AssignUserToRole(string roleName, string userName);

        void AssignGroupToRole(string roleName, string groupName);

        void RemoveUserFromRole(string roleName, string userName);

        void RemoveGroupFromRole(string roleName, string groupName);
        
        void RemoveRole(string roleName);
    }
}
