﻿namespace Base2art.Security.Characterization.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using Models;
    using Base2art.Collections.Specialized;

    public class JsonCharacterizationRepository : ICharacterizationRepository
    {
        private KeyedCollection<string, Role> roles = new MyKeyedCollection();

        private readonly string roleFilePath;

        public JsonCharacterizationRepository(string roleFilePath)
        {
            this.roleFilePath = roleFilePath;
            this.Reset();
        }

        public IPagedCollection<IRole> Roles(Page page)
        {
            var currentRoles = this.roles;
            var pageData = currentRoles
                .OrderBy(x=> x.Name)
                .Skip(page.Start())
                .Take(page.PageSize);
            return new PagedCollection<IRole>(pageData, page, currentRoles.Count);
        }
        
        public string[] UserRoles(string userName)
        {
            return roles
                .Where(x => ChildUsers(x).Contains(userName))
                .Select(x => x.Name)
                .ToArray();
        }

        public string[] GroupRoles(string groupName)
        {
            return roles
                .Where(x => ChildGroups(x).Contains(groupName))
                .Select(x => x.Name)
                .ToArray();
        }

        public IRole FindRole(string name)
        {
            return this.FindRoleByName(name);
        }
        
        public void AddRole(string name, string description)
        {
            var role = this.FindRoleByName(name);
            if (role != null)
            {
                return;
            }
            
            this.StoreItem(new Role
                           {
                               Name = name,
                               Description = description,
                               Groups = new string[0],
                               Users = new string[0]
                           });
        }

        public void UpdateRoleByName(string name, string description)
        {
            var role = this.FindRoleByName(name);
            if (role == null)
            {
                return;
            }
            
            this.StoreItem(new Role
                           {
                               Name = role.Name,
                               Description = description,
                               Groups = role.Groups,
                               Users = role.Users
                           });
        }
        
        public void AssignUserToRole(string roleName, string userName)
        {
            var role = this.FindRoleByName(roleName);
            if (role == null)
            {
                return;
            }
            
            var childUsers = new HashSet<string>(role.Users ?? new string[0]);
            childUsers.Add(userName);
            role.Users = childUsers.ToArray();
            this.StoreItem(role);
        }

        public void AssignGroupToRole(string roleName, string groupName)
        {
            var role = this.FindRoleByName(roleName);
            if (role == null)
            {
                return;
            }
            
            var childGroups = new HashSet<string>(role.Groups ?? new string[0]);
            childGroups.Add(groupName);
            role.Groups = childGroups.ToArray();
            this.StoreItem(role);
        }

        public void RemoveUserFromRole(string roleName, string userName)
        {
            var role = this.FindRoleByName(roleName);
            if (role == null)
            {
                return;
            }
            
            var childUsers = new HashSet<string>(role.Users ?? new string[0]);
            childUsers.Remove(userName);
            role.Users = childUsers.ToArray();
            this.StoreItem(role);
        }

        public void RemoveGroupFromRole(string roleName, string groupName)
        {
            var role = this.FindRoleByName(roleName);
            if (role == null)
            {
                return;
            }
            
            var childGroups = new HashSet<string>(role.Groups ?? new string[0]);
            childGroups.Remove(groupName);
            role.Groups = childGroups.ToArray();
            this.StoreItem(role);
        }
        
        public void RemoveRole(string roleName)
        {
            var role = this.FindRoleByName(roleName);
            if (role == null)
            {
                return;
            }
            
            this.DeleteItem(role);
        }
        
        protected Role FindRoleByName(string name)
        {
            try
            {
                return this.roles[name];
            }
            catch (Exception)
            {
                return null;
            }
        }
        
        private void DeleteItem(Role role)
        {
            var file = File.ReadAllText(this.roleFilePath);
            var serializier = new Serialization.NewtonsoftSerializer();
            var currentRoles = serializier.Deserialize<List<Role>>(file);
            currentRoles.RemoveAll(x => string.Equals(x.Name, role.Name, StringComparison.OrdinalIgnoreCase));
            
            var output = serializier.Serialize<List<Role>>(currentRoles);
            File.WriteAllText(this.roleFilePath, output);
            this.Reset();
        }
        
        private void StoreItem(Role role)
        {
            var file = File.ReadAllText(this.roleFilePath);
            var serializier = new Serialization.NewtonsoftSerializer();
            var currentRoles = serializier.Deserialize<List<Role>>(file);
            
            currentRoles.RemoveAll(x => string.Equals(x.Name, role.Name, StringComparison.OrdinalIgnoreCase));
            currentRoles.Add(role);
            
            var output = serializier.Serialize<List<Role>>(currentRoles);
            File.WriteAllText(this.roleFilePath, output);
            
            this.Reset();
        }

        private static HashSet<string> ChildUsers(Role role)
        {
            return new HashSet<string>(role.Users ?? new string[0], StringComparer.OrdinalIgnoreCase);
        }

        private static HashSet<string> ChildGroups(Role role)
        {
            return new HashSet<string>(role.Groups ?? new string[0], StringComparer.OrdinalIgnoreCase);
        }

        private void Reset()
        {
            if (!File.Exists(this.roleFilePath))
            {
                File.WriteAllText(this.roleFilePath, "[]");
            }
            
            var file = File.ReadAllText(this.roleFilePath);
            var serializier = new Serialization.NewtonsoftSerializer();
            var currentUsers = serializier.Deserialize<List<Role>>(file);
            var collection = new MyKeyedCollection();
            
            foreach (var current in currentUsers)
            {
                collection.Add(current);
            }
            
            this.roles = collection;
        }
        
        private class MyKeyedCollection : KeyedCollection<string, Role>
        {
            protected override string GetKeyForItem(Role item)
            {
                return item.Name;
            }
        }
    }
}
