﻿namespace Base2art.Security.AccessControl
{
    using System.Collections.Generic;
    using Base2art.Security.Authentication;
	public static class SecurityStatusMapper
	{
		private readonly static Dictionary<SignInStatus, SecurityStatus> mapper = new Dictionary<SignInStatus, SecurityStatus> {
			{
				SignInStatus.BadPassword,
				SecurityStatus.BadPassword
			},
			{
				SignInStatus.InvalidRequest,
				SecurityStatus.InvalidRequest
			},
			{
				SignInStatus.LockedOut,
				SecurityStatus.LockedOut
			},
			{
				SignInStatus.Success,
				SecurityStatus.Success
			},
			{
				SignInStatus.UnknownUser,
				SecurityStatus.UnknownUser
			},
		};

		public static SecurityStatus MapToSecurityStatus(this SignInStatus status)
		{
			return mapper.ContainsKey(status) ? mapper[status] : SecurityStatus.UnknownError;
		}
	}
}


