﻿namespace Base2art.Security.AccessControl
{
	using System;

	public interface IResponseDataAccessor
	{
		void Header(string authorizationHeader, string value);

		void Cookie(string cookieName, string value);

		void Uri(string uri);
	}
}


