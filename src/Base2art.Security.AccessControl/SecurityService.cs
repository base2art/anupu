﻿namespace Base2art.Security.AccessControl
{
    using System;
    using Base2art.Security.Authentication;
    using Base2art.Security.Authorization;
    using Base2art.Security.Characterization;
    using Base2art.Security.Membership;
    
    public class SecurityService : ISecurityService
    {
        private readonly ISecurityServiceSettings settings;

        private readonly ISecurityProcessorFactory authHandlerFactory;

        private readonly IAuthorizationProcessor authorizationProcessor;

        private readonly IAuthenticationProcesssor authenticationProcessor;

        private readonly IMembershipProcessor membershipProcessor;

        private readonly ICharacterizationProcessor characterizationProcessor;
        
        public SecurityService(
            ISecurityServiceSettings settings,
            IAuthenticationProcesssor authenticationProcessor,
            IMembershipProcessor membershipProcessor,
            ICharacterizationProcessor characterizationProcessor,
            IAuthorizationProcessor authorizationProcessor,
            ISecurityProcessorFactory authHandlerFactory)
        {
            this.characterizationProcessor = characterizationProcessor;
            this.membershipProcessor = membershipProcessor;
            this.authenticationProcessor = authenticationProcessor;
            this.authorizationProcessor = authorizationProcessor;
            this.authHandlerFactory = authHandlerFactory;
            this.settings = settings;
        }

        public IAuthenticationProcesssor AuthenticationProcessor
        {
            get { return this.authenticationProcessor; }
        }

        public ICharacterizationProcessor CharacterizationProcessor
        {
            get { return this.characterizationProcessor; }
        }

        public IMembershipProcessor MembershipProcessor
        {
            get { return this.membershipProcessor; }
        }

        public IAuthorizationProcessor AuthorizationProcessor
        {
            get { return this.authorizationProcessor; }
        }
        
        public SecurityStatus CanAccess(IRequestDataAccessor request, string requiredRole)
        {
            var authHeader = request.Header(this.settings.HeaderName);
            var authCookie = request.Cookie(this.settings.CookieName);
            
            ISecurityProcessorResult result = null;
            if (!string.IsNullOrWhiteSpace(authHeader) && !string.IsNullOrWhiteSpace(authCookie))
            {
                var authHandler = authHandlerFactory.CreateMalformedRequestRoleBasedProcessor(
                    this.authenticationProcessor,
                    this.membershipProcessor,
                    this.characterizationProcessor,
                    this.authorizationProcessor);
                result = authHandler.ProcessMalformedRequestRoleBased(requiredRole);
            }
            
            
            if (string.IsNullOrWhiteSpace(authHeader) && string.IsNullOrWhiteSpace(authCookie))
            {
                var authHandler = authHandlerFactory.CreateNoAuthTokenRequestRoleBasedProcessor(
                    this.authenticationProcessor,
                    this.membershipProcessor,
                    this.characterizationProcessor,
                    this.authorizationProcessor);
                result = authHandler.ProcessNoAuthRoleBased(requiredRole);
            }
            
            if (string.IsNullOrWhiteSpace(authHeader) && !string.IsNullOrWhiteSpace(authCookie))
            {
                var authHandler = authHandlerFactory.CreateCookieRoleBasedProcessor(
                    this.authenticationProcessor,
                    this.membershipProcessor,
                    this.characterizationProcessor,
                    this.authorizationProcessor);
                result = authHandler.ProcessCookieRoleBased(authCookie, requiredRole);
            }
            
            if (!string.IsNullOrWhiteSpace(authHeader) && string.IsNullOrWhiteSpace(authCookie))
            {
                var authHandler = authHandlerFactory.CreateHeaderRoleBasedProcessor(
                    this.authenticationProcessor,
                    this.membershipProcessor,
                    this.characterizationProcessor,
                    this.authorizationProcessor);
                result = authHandler.ProcessHeaderRoleBased(authHeader, requiredRole);
            }
            
            // Do I need to set headers???
            
            return result.Status;
        }

        public SecurityStatus CanAccess(IRequestDataAccessor request, IResponseDataAccessor response)
        {
            var authHeader = request.Header(this.settings.HeaderName);
            var authCookie = request.Cookie(this.settings.CookieName);
            
            ISecurityProcessorResult result = null;
            if (!string.IsNullOrWhiteSpace(authHeader) && !string.IsNullOrWhiteSpace(authCookie))
            {
                var authHandler = authHandlerFactory.CreateMalformedRequestProcessor(
                    this.authenticationProcessor,
                    this.membershipProcessor,
                    this.characterizationProcessor,
                    this.authorizationProcessor);
                result = authHandler.ProcessMalformedRequest(request.Uri());
            }
            
            
            if (string.IsNullOrWhiteSpace(authHeader) && string.IsNullOrWhiteSpace(authCookie))
            {
                var authHandler = authHandlerFactory.CreateNoAuthTokenRequestProcessor(
                    this.authenticationProcessor,
                    this.membershipProcessor,
                    this.characterizationProcessor,
                    this.authorizationProcessor);
                result = authHandler.ProcessNoAuth(request.Uri());
            }
            
            if (string.IsNullOrWhiteSpace(authHeader) && !string.IsNullOrWhiteSpace(authCookie))
            {
                var authHandler = authHandlerFactory.CreateCookieProcessor(
                    this.authenticationProcessor,
                    this.membershipProcessor,
                    this.characterizationProcessor,
                    this.authorizationProcessor);
                result = authHandler.ProcessCookie(authCookie, request.Uri());
            }
            
            if (!string.IsNullOrWhiteSpace(authHeader) && string.IsNullOrWhiteSpace(authCookie))
            {
                var authHandler = authHandlerFactory.CreateHeaderProcessor(
                    this.authenticationProcessor,
                    this.membershipProcessor,
                    this.characterizationProcessor,
                    this.authorizationProcessor);
                result = authHandler.ProcessHeader(authHeader, request.Uri());
            }
            
            if (!string.IsNullOrWhiteSpace(result.HeaderKey) && !string.IsNullOrWhiteSpace(result.HeaderValue))
            {
                response.Header(result.HeaderKey, result.HeaderValue);
            }
            
            if (!string.IsNullOrWhiteSpace(result.CookieKey) && !string.IsNullOrWhiteSpace(result.CookieValue))
            {
                response.Cookie(result.HeaderKey, result.HeaderValue);
            }
            
            return result.Status;
        }
    }
}