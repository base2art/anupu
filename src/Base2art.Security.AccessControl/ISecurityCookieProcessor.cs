﻿namespace Base2art.Security.AccessControl
{
	using System;

	public interface ISecurityCookieProcessor
	{
		ISecurityProcessorResult ProcessCookie(string cookieValue, Uri uri);
	}
}


