﻿namespace Base2art.Security.AccessControl
{
    public class SecurityServiceSettings : ISecurityServiceSettings
    {
        private readonly string cookieName;

        private readonly string headerName;
        
        
        public SecurityServiceSettings()
            : this(".b2a.auth", "Authorization")
        {
        }

        public SecurityServiceSettings(string cookieName, string headerName)
        {
            this.headerName = headerName;
            this.cookieName = cookieName;
        }

        public string CookieName
        {
            get { return this.cookieName; }
        }

        public string HeaderName
        {
            get { return this.headerName; }
        }
    }
}


