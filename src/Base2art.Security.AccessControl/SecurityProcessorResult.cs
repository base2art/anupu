﻿namespace Base2art.Security.AccessControl
{
	using System;
    using Base2art.Security.AccessControl;

	public class SecurityProcessorResult : ISecurityProcessorResult
	{
		private readonly SecurityStatus status;

		private readonly string user = null;

		private readonly string[] roles = new string[0];

		public SecurityProcessorResult(SecurityStatus status)
		    : this(status, null, null)
		{
		}

		public SecurityProcessorResult(SecurityStatus status, string user, string[] roles)
		{
            this.user = user;
            this.roles = roles ?? new string[0];
			this.status = status;
		}

		public string User
		{
			get { return this.user; }
		}

		public string[] Roles
		{
			get { return this.roles; }
		}

		public SecurityStatus Status
		{
			get { return this.status; }
		}

		public string HeaderKey { get; set; }

        public string HeaderValue { get; set; }

        public string CookieKey { get; set; }

        public string CookieValue { get; set; }
	}
}


