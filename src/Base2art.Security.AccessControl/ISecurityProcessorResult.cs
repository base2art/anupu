﻿namespace Base2art.Security.AccessControl
{
	public interface ISecurityProcessorResult
	{
		string User { get; }

		string[] Roles { get; }

		SecurityStatus Status { get; }
		
		string HeaderKey { get; }
		
		string HeaderValue { get; }
		
		string CookieKey { get; }
		
		string CookieValue { get; }
	}
}
