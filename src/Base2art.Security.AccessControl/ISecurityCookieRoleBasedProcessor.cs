﻿namespace Base2art.Security.AccessControl
{
	using System;

	public interface ISecurityCookieRoleBasedProcessor
	{
		ISecurityProcessorResult ProcessCookieRoleBased(string cookieValue, string requiredRole);
	}
}




