﻿namespace Base2art.Security.AccessControl
{
	using System;

	public interface ISecurityRoleBasedProcessor
	{
		ISecurityProcessorResult ProcessRoleBasedRequest(string userName, string requiredRole);

        ISecurityProcessorResult ProcessMalformedRoleBasedRequest(string requiredRole);
	}
}




