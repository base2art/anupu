﻿namespace Base2art.Security.AccessControl
{
    using System;
    
    public interface ISecurityHeaderProcessor
	{
	    ISecurityProcessorResult ProcessHeader(string headerValue, Uri uri);
	}
}
