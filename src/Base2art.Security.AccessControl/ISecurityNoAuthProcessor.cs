﻿namespace Base2art.Security.AccessControl
{
	using System;

	public interface ISecurityNoAuthProcessor
	{
        ISecurityProcessorResult ProcessNoAuth(Uri uri);
	}
}
