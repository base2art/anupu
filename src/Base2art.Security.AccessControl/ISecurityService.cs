﻿namespace Base2art.Security.AccessControl
{
    using Base2art.Security.Authentication;
    using Base2art.Security.Authorization;
    using Base2art.Security.Characterization;
    using Base2art.Security.Membership;
    
	public interface ISecurityService
	{
        IAuthenticationProcesssor AuthenticationProcessor { get; }

        ICharacterizationProcessor CharacterizationProcessor { get; }

        IMembershipProcessor MembershipProcessor { get; }

        IAuthorizationProcessor AuthorizationProcessor { get; }
	    
		SecurityStatus CanAccess(IRequestDataAccessor request, IResponseDataAccessor response);
		
		SecurityStatus CanAccess(IRequestDataAccessor request, string requiredRole);
	}
}

