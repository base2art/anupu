﻿namespace Base2art.Security.AccessControl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base2art.Security.Characterization;
    using Base2art.Security.Membership;
    
    public static class RoleProvider
    {
        public static string[] GetRoles(
            IMembershipProcessor membershipProcessor,
            ICharacterizationProcessor characterizationProcessor,
            string userName)
        {
            var groups = membershipProcessor.UserGroups(userName);
            var roles = new HashSet<string>(characterizationProcessor.UserRoles(userName) ?? new string[0], StringComparer.OrdinalIgnoreCase);
            var groupRoles = groups.SelectMany(group => characterizationProcessor.GroupRoles(group));
            
            foreach (var groupRole in groupRoles)
            {
                roles.Add(groupRole);
            }
            
            return roles.ToArray();
        }
    }
}
