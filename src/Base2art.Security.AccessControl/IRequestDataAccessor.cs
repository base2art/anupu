﻿namespace Base2art.Security.AccessControl
{
    using System;
    
    public interface IRequestDataAccessor
    {
        string Header(string authorizationHeader);

        string Cookie(string cookieName);
        
        Uri Uri();
    }
}
