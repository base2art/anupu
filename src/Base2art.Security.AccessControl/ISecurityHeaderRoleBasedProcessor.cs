﻿namespace Base2art.Security.AccessControl
{
	using System;

	public interface ISecurityHeaderRoleBasedProcessor
	{
		ISecurityProcessorResult ProcessHeaderRoleBased(string headerValue, string requiredRole);
	}
}


