﻿namespace Base2art.Security.AccessControl
{
    public interface ISecurityServiceSettings
    {
        //"Authorization"
        string HeaderName { get; }
        string CookieName { get; }
    }
}
