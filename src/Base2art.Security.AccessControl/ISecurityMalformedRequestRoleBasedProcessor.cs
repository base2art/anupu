﻿namespace Base2art.Security.AccessControl
{
	using System;

	public interface ISecurityMalformedRequestRoleBasedProcessor
	{
		ISecurityProcessorResult ProcessMalformedRequestRoleBased(string requiredRole);
	}
}




