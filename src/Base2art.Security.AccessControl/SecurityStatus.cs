﻿namespace Base2art.Security.AccessControl
{
    public enum SecurityStatus
    {
        UnknownError,
        Success,
        NoAccess,
        InvalidRequest,
        UnknownUser,
        BadPassword,
        LockedOut,
    }
    
    
}
