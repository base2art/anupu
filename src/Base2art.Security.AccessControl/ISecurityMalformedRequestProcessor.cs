﻿namespace Base2art.Security.AccessControl
{
	using System;

	public interface ISecurityMalformedRequestProcessor
	{
        ISecurityProcessorResult ProcessMalformedRequest(Uri uri);
	}
}


