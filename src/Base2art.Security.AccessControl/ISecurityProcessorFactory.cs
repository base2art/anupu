﻿namespace Base2art.Security.AccessControl
{
    using Base2art.Security.Authentication;
    using Base2art.Security.Authorization;
    using Base2art.Security.Characterization;
    using Base2art.Security.Membership;
    
	public interface ISecurityProcessorFactory
	{
        ISecurityHeaderProcessor CreateHeaderProcessor(
	        IAuthenticationProcesssor authenticationProcessor, 
	        IMembershipProcessor membershipProcessor, 
	        ICharacterizationProcessor characterizationProcessor,
	        IAuthorizationProcessor authorizationProcessor);
        
        ISecurityCookieProcessor CreateCookieProcessor(
	        IAuthenticationProcesssor authenticationProcessor, 
	        IMembershipProcessor membershipProcessor, 
	        ICharacterizationProcessor characterizationProcessor, 
	        IAuthorizationProcessor authorizationProcessor);

        ISecurityNoAuthProcessor CreateNoAuthTokenRequestProcessor(
	        IAuthenticationProcesssor authenticationProcessor, 
	        IMembershipProcessor membershipProcessor, 
	        ICharacterizationProcessor characterizationProcessor, 
	        IAuthorizationProcessor authorizationProcessor);

        ISecurityMalformedRequestProcessor CreateMalformedRequestProcessor(
	        IAuthenticationProcesssor authenticationProcessor, 
	        IMembershipProcessor membershipProcessor, 
	        ICharacterizationProcessor characterizationProcessor, 
	        IAuthorizationProcessor authorizationProcessor);

	    
	    //
	    
        ISecurityHeaderRoleBasedProcessor CreateHeaderRoleBasedProcessor(
	        IAuthenticationProcesssor authenticationProcessor, 
	        IMembershipProcessor membershipProcessor, 
	        ICharacterizationProcessor characterizationProcessor,
	        IAuthorizationProcessor authorizationProcessor);
        
        ISecurityCookieRoleBasedProcessor CreateCookieRoleBasedProcessor(
	        IAuthenticationProcesssor authenticationProcessor, 
	        IMembershipProcessor membershipProcessor, 
	        ICharacterizationProcessor characterizationProcessor, 
	        IAuthorizationProcessor authorizationProcessor);

        ISecurityNoAuthRoleBasedProcessor CreateNoAuthTokenRequestRoleBasedProcessor(
	        IAuthenticationProcesssor authenticationProcessor, 
	        IMembershipProcessor membershipProcessor, 
	        ICharacterizationProcessor characterizationProcessor, 
	        IAuthorizationProcessor authorizationProcessor);

        ISecurityMalformedRequestRoleBasedProcessor CreateMalformedRequestRoleBasedProcessor(
	        IAuthenticationProcesssor authenticationProcessor, 
	        IMembershipProcessor membershipProcessor, 
	        ICharacterizationProcessor characterizationProcessor, 
	        IAuthorizationProcessor authorizationProcessor);
	    
	    /*
        ISecurityRoleBasedProcessor CreateNoAuthTokenRoleBasedRequestProcessor(
	        IAuthenticationProcesssor authenticationProcessor, 
	        IMembershipProcessor membershipProcessor, 
	        ICharacterizationProcessor characterizationProcessor, 
	        IAuthorizationProcessor authorizationProcessor);

        ISecurityRoleBasedProcessor CreateMalformedRoleBasedRequestProcessor(
	        IAuthenticationProcesssor authenticationProcessor, 
	        IMembershipProcessor membershipProcessor, 
	        ICharacterizationProcessor characterizationProcessor, 
	        IAuthorizationProcessor authorizationProcessor);

        ISecurityRoleBasedProcessor CreateHeaderRoleBasedProcessor(
	        IAuthenticationProcesssor authenticationProcessor, 
	        IMembershipProcessor membershipProcessor, 
	        ICharacterizationProcessor characterizationProcessor, 
	        IAuthorizationProcessor authorizationProcessor);

        ISecurityRoleBasedProcessor CreateCookieRoleBasedProcessor(
	        IAuthenticationProcesssor authenticationProcessor, 
	        IMembershipProcessor membershipProcessor, 
	        ICharacterizationProcessor characterizationProcessor, 
	        IAuthorizationProcessor authorizationProcessor);
	        */
	}
}


