﻿namespace Base2art.Security.AccessControl
{
	using System;

	public interface ISecurityNoAuthRoleBasedProcessor
	{
        ISecurityProcessorResult ProcessNoAuthRoleBased(string requiredRole);
	}
}


